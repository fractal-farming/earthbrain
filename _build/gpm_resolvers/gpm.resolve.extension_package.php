<?php
/**
 * Resolve creating db tables
 *
 * THIS RESOLVER IS AUTOMATICALLY GENERATED, NO CHANGES WILL APPLY
 *
 * @package earthbrain
 * @subpackage build
 *
 * @var mixed $object
 * @var modX $modx
 * @var array $options
 */

if ($object->xpdo) {
    $modx =& $object->xpdo;
    switch ($options[xPDOTransport::PACKAGE_ACTION]) {
        case xPDOTransport::ACTION_INSTALL:
        case xPDOTransport::ACTION_UPGRADE:
            $modelPath = $modx->getOption('earthbrain.core_path');

            if (empty($modelPath)) {
                $modelPath = '[[++core_path]]components/earthbrain/model/';
            }

            if ($modx instanceof modX) {
                $modx->addExtensionPackage('earthbrain', $modelPath, array (
  'serviceName' => 'earthbrain',
  'serviceClass' => 'EarthBrain',
));
            }

            break;
        case xPDOTransport::ACTION_UNINSTALL:
            if ($modx instanceof modX) {
                $modx->removeExtensionPackage('earthbrain');
            }

            break;
    }
}

return true;