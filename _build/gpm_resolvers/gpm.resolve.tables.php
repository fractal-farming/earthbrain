<?php
/**
 * Resolve creating db tables
 *
 * THIS RESOLVER IS AUTOMATICALLY GENERATED, NO CHANGES WILL APPLY
 *
 * @package earthbrain
 * @subpackage build
 *
 * @var mixed $object
 * @var modX $modx
 * @var array $options
 */

if ($object->xpdo) {
    $modx =& $object->xpdo;
    switch ($options[xPDOTransport::PACKAGE_ACTION]) {
        case xPDOTransport::ACTION_INSTALL:
        case xPDOTransport::ACTION_UPGRADE:
            $modelPath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/') . 'model/';
            
            $modx->addPackage('earthbrain', $modelPath, null);


            $manager = $modx->getManager();

            $manager->createObjectContainer('earthImage');
            $manager->createObjectContainer('earthImageTag');
            $manager->createObjectContainer('earthNote');
            $manager->createObjectContainer('earthLink');
            $manager->createObjectContainer('earthTaxon');
            $manager->createObjectContainer('earthSeed');
            $manager->createObjectContainer('earthPlant');
            $manager->createObjectContainer('earthPlanting');
            $manager->createObjectContainer('earthPlantingFeature');
            $manager->createObjectContainer('earthPersonData');
            $manager->createObjectContainer('earthSource');
            $manager->createObjectContainer('earthExchange');
            $manager->createObjectContainer('earthExchangeItem');
            $manager->createObjectContainer('earthAddress');
            $manager->createObjectContainer('earthLocation');

            break;
    }
}

return true;