<?php
/**
 * Merge JSON configurations for GPM into single config file.
 *
 * It allows for template assignment to be referenced by a variable.
 * Create and maintain these template lists in the src_snippets folder, and then
 * reference them by prefixing their file name with @@. Make sure it aligns with
 * the JSON formatting inside the parent object!
 *
 * Update January 2023:
 * Functionality extended to include MIGX configs.
 */

include_once 'merge-json.config.php';

$corePath = MODX_BASE_PATH. 'packages/' . GPM_PACKAGE . '/core/components/' . GPM_PACKAGE . '/';
$buildPath = MODX_BASE_PATH. 'packages/' . GPM_PACKAGE . '/_build/';

// Use external library for validating JSON
require_once $corePath . 'vendor/autoload.php';
use Seld\JsonLint\JsonParser;
use Seld\JsonLint\ParsingException;

$parser = new JsonParser();

$verbose = false;
$options = getopt('', ['verbose']);
if (isset($options['verbose'])) {
    $verbose = true;
}

// GPM CONFIG
// -----------------------------------------------------------------------------

// Get all JSON files from src folder
$sources = glob("{{$buildPath}src/*.json,{$buildPath}src/*/*.json,{$buildPath}src/*/*/*.json}", GLOB_BRACE);

// Get template lists for linking TV assignments in bulk
$templateList = glob("{src_snippets/*}", GLOB_BRACE);

// Set base of config file
$baseArray = json_decode(file_get_contents($buildPath . 'src/base.json'), true);
$sourceArray = [];

foreach ($sources as $index => $source) {
    $json = file_get_contents($source);
    $validateSource = $parser->lint($json);

    // Check if file content is valid JSON
    if ($validateSource) {
        echo $validateSource . "\n";
        echo "Source: $source \n";
        echo "Validation failed. \n";
        return false;
    } elseif ($verbose) {
        echo "$source is valid JSON. \n";
    }

    // Skip base file
    if (strpos($source, 'base.json')) {
        continue;
    }

    // Decode file as associative array
    $sourceArray = json_decode($json, true);

    // Append this data to the main array on each loop
    $baseArray = array_merge_recursive($baseArray,$sourceArray);
}

$output = json_encode($baseArray, JSON_PRETTY_PRINT);

// Fill placeholders with corresponding template names
foreach ($templateList as $file) {
    $list = file_get_contents($file);
    $list = str_replace(array("\r", "\n"), '', $list);
    $filename = pathinfo($file)['filename'];

    // Insert list wherever filename matches an @@template_list value
    $output = str_replace('@@' . $filename, $list, $output);
}

// Validate output
$validateOutput = $parser->lint($output);
if ($validateOutput) {
    echo $validateOutput . "\n";
    echo "Validation of GPM config failed.";
    return false;
} elseif ($verbose) {
    echo "GPM config file is valid JSON. \n";
}

// Write the config file
file_put_contents($buildPath . 'config.json', $output);
echo "GPM config successfully written. \n";


// MIGX CONFIGS
// -----------------------------------------------------------------------------

// Get all JSON files from migx folder
$configs = glob("{{$buildPath}migx/*.json}", GLOB_BRACE);

// Get variables
$lexicon = file_get_contents($buildPath . 'migx/variables/lexicon.json');
$lexicon = json_decode($lexicon,1);
//$settings = file_get_contents($buildPath . 'migx/variables/settings.json');

// Fill placeholders with corresponding templates
foreach ($configs as $config) {
    $json = file_get_contents($config);
    $json = preg_replace_callback('("@@([a-zA-Z/_-]+)")', function ($matches) use ($buildPath) {
        $basePath = $buildPath . 'migx/';
        $filePath = $matches[1];
        if (str_starts_with($filePath, 'earthbrain')) {
            $basePath = MODX_BASE_PATH . 'packages/earthbrain/_build/migx/';
            $filePath = str_replace('earthbrain/', '', $filePath);
        }
        $file = $basePath . $filePath . '.json';
        return file_get_contents($file);
    }, $json);

    // Lexicon strings, why not
    $json = preg_replace_callback('("%%([a-zA-Z0-9/._-]+)")', function ($matches) use ($lexicon) {
        $string = $matches[1];
        if (str_starts_with($string, 'earthbrain')) {
            $lexicon = file_get_contents(MODX_BASE_PATH . 'packages/earthbrain/_build/migx/variables/lexicon.json');
            $lexicon = json_decode($lexicon,1);
        }
        foreach ($lexicon as $item) {
            if ($item['key'] == $string) {
                return json_encode($item['value']);
            }
        }
        return false;
    }, $json);

    // Check if result is valid JSON
    $validateConfig = $parser->lint($json);
    if ($validateConfig) {
        echo $validateConfig . "\n";
        echo "Config: $config \n";
        echo "Validation failed. \n";
        return false;
    } elseif ($verbose) {
        echo "$config is valid JSON. \n";
    }

    // Pretty ugly, is what this is...
    $array = json_decode($json, true);
    $json = json_encode($array, JSON_PRETTY_PRINT);

    // Write to migxconfigs folder
    $file = basename($config, '.json');
    $path = $corePath . 'migxconfigs/' . $file . '.config.js';
    file_put_contents($path, $json);
}

echo "MIGX configs successfully written. \n";
return true;