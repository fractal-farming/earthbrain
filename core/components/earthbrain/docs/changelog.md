# Changelog for EarthBrain

## EarthBrain 0.5.2
Released on January 25, 2025

Fixes and improvements:
- Fix transport package installation (autoloader is not yet available)

## EarthBrain 0.5.1
Released on October 28, 2024

Fixes and improvements:
- Set default address country code with system setting
- Use filename as image title if not explicitly set
- Remove migxGetClassKeys snippet (and fetch class keys from EarthBrain class)
- Move classes containing functional code to namespaced src folder
- Move MIGX properties to EarthBrain constructor
- Add zoom and KML fields to locations table
- Rename all overviewRowPerson elements to overviewRowEarthling
- Limit people overviews to allowed context member groups
- Limit MIGX grids to items created by logged in user (except for sudo users)

## EarthBrain 0.5.0
Released on August 6, 2024

New features:
- Add elements for displaying person data in overviews and on map
- Add ability to send activation emails to alternative address
- Add plugin to resize images on upload
- Add ability to (reverse) geocode addresses from grid window
- Extract location data from image EXIF data

Fixes and improvements:
- Segment custom cache based on user access level
- Set user group from modal when saving person
- Only format phone numbers if value is set
- Remove empty {id} placeholders from symlink media paths
- Accept placeholder for object ID in media source paths
- Connect links to online source type (instead of reverse)
- Adapt saveAddress function to use processors
- Adapt saveLocation function to use processors
- Add EarthBrain class for incrementing grid position
- Add settings tab to modals
- Add date taken field to Images
- Manage all privacy settings in separate MIGX tab
- Use lexicon strings in MIGX configs
- Add date and published fields to Image object
- Allow negative value for Location elevation setting
- Add comment field to Exchange Items
- Add ability to attach notes and images to an Exchange
- Add option to label an Exchange as gift
- Remove duplicate composite key for Exchange object
- Set minimum PHP version to 8.1

## EarthBrain 0.4.0
Released on October 8, 2023

New features:
- Add ability to tag Images [WIP]
- Add ability to record Exchanges (transactions)
- Use junction table to store fields with multiple values
- Add taxon height and radius fields
- Add plant residence and features fields

Fixes and improvements:
- Add create/update processors for Address and Location objects
- Don't set default class_key of Plant object in schema
- Hide Seeds and Sources created by other users
- Add search field to Plant, People and Seed grids
- Tweaks and fixes to source path symlink creation
- Generate correct media source paths for forest Components and Features
- Ensure objects are saved in MIGX hooks
- Include Images, Links and Notes in extract
- Catch classKey exceptions when fixing source path
- Show thumbnail in image selectors
- Remove HTML tags from selected value in combo boxes
- Remove inline styles from floated grid icons (now defined in CSS)
- Tweak varchar fields with sensible maximum values

## EarthBrain 0.3.1
Released on May 24, 2023

Fixes and improvements:
- Refactor earthSource object
- Manage person privacy settings in separate tab

## EarthBrain 0.3.0
Released on March 10, 2023

New features:
- Add snippet for importing GeoJSON data
- Add ability to reverse geocode coordinates
- Add basic templating engine for MIGX configs
- Add processors for creating Image, Link and Node objects
- Add processors for creating Address, Location and Person objects

Fixes and improvements:
- Add ability to run merge-json.php from inside other packages
- Add function to generate image variants
- Add function to correct media source path for images
- Replace hardcoded media source IDs with system setting
- Remove hardcoded Philippines string from addresses
- Add function to find user by phone number
- Add function to reformat phone number

## EarthBrain 0.2.0
Released on December 6, 2022

New features:
- Add geocoding functionality

Fixes and improvements:
- Accept fullname value from user input
- Return ID in result array after creating new user
- Remove Guzzle7 MODX dependency
- Autoload Composer dependencies during package install
- Add package and default properties to TV options selector
- Remove Organization and Jobtitle fields from person data
- Fix address object always being created because of prefilled country field
- Replace all references to Contact with Person
- Add grid actions with edit button
- Make it possible to attach images to resources
- Prevent PHP 8 warnings when reading possibly undefined keys in request array
- Add option to set start zoom level of map

## EarthBrain 0.1.0
Released on August 25, 2022

Earthbrain contains generic types of data, which can be used (and extended) by
other packages.

New features:
- Create symlinks to plant images in context image folder
- Autofill image variants when creating new image
- Extend Images, Links and Notes for each class
- Add table and grid for Links
- Add table and grid for Notes
- Add reusable MIGX grid for Images
- Add separate table for Images

Fixes and improvements:
- Add option to set start zoom level of map
- Share image folder between Plantings and Plants
- Change plantItems into Plantings
- Use single media source for all images
- Rename Users to Person / People
- Rename Species class to Taxon
- Import functions and elements from FoodBrain

Tables migrated from FoodBrain:
- Species (now Taxon)
- Seed
- Plant
- PlantItem (now Planting)
- User (now Person)
- Source
- Address
- Location