[[[[Switch?
    &get=`[[+class_key]]`

    &c1=`earthExchangePersonPerson` &do1=`$migxGridPerson? &person_id=`[[+parent_id]]` &idx=`[[+idx]]``
    &c2=`earthExchangePersonSource` &do2=`$migxGridPerson? &person_id=`[[+parent_id]]` &idx=`[[+idx]]``
    &c3=`earthExchangeSourcePerson` &do3=`$migxGridSource? &source_id=`[[+parent_id]]` &idx=`[[+idx]]``

    &default=`$migxGridExchangeParentNameAdditional? &idx=`[[+idx]]``
]]]]