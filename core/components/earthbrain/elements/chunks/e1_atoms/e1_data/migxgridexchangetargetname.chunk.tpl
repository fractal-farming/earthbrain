[[[[Switch?
    &get=`[[+class_key]]`

    &c1=`earthExchangePersonPerson` &do1=`$migxGridPerson? &person_id=`[[+target_id]]` &idx=`[[+idx]]``
    &c2=`earthExchangePersonSource` &do2=`$migxGridSource? &source_id=`[[+target_id]]` &idx=`[[+idx]]``
    &c3=`earthExchangeSourcePerson` &do3=`$migxGridPerson? &person_id=`[[+target_id]]` &idx=`[[+idx]]``

    &default=`$migxGridExchangeTargetNameAdditional? &idx=`[[+idx]]``
]]]]