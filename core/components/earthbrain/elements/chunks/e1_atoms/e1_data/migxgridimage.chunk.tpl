[[+class:replaceRegex=`^[a-z]+Image`:lcase:replace=`image==`:toPlaceholder=`category_[[+uid]]`]]
[[modifiedIf?
    &subject=`[[+class]]`
    &operator=`is`
    &operand=`earthImagePlanting`
    &then=`[[migxLoopCollection?
            &packageName=`earthbrain`
            &classname=`earthPlanting`
            &where=`{"id":"[[+parent_id]]"}`
            &tpl=`@CODE:plant/[[+plant_id]]/`
            &limit=`1`
        ]]`
    &else=`[[+category_[[+uid]]]]/[[+parent_id]]/`
    &toPlaceholder=`path_[[+uid]]`
]]
[[If
    :replace=`"source": [[+source:default=`[[++earthbrain.img_source]]`]]=="source": 1`
    :replace=`"src": "=="src": "/uploads/img/[[+path_[[+uid]]]]`
    :toPlaceholder=`img_[[+uid]]`?
    &subject=`[[+img]]`
    &operator=`empty`
    &then=`
        [[migxLoopCollection?
            &packageName=`earthbrain`
            &classname=`earthImage`
            &where=`[{"class_key":"[[+class]]"},{"parent_id":"[[+id]]"},{"deleted:=":0}]`
            &tpl=`@CODE:[[+img]]`
            &limit=`1`
            &sortConfig=`[{"sortby":"pos","sortdir":"ASC"}]`
        ]]`
    &else=`[[+img]]`
]]
<img src="[[ImagePlus? &options=`w=[[+width:default=`480`]]` &value=`[[+img_[[+uid]]]]`]]" style="max-width:[[+max_width:default=`100%`]];height:auto;">