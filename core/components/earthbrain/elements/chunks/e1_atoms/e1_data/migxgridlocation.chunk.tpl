[[+Location_lat:notempty=`
<i class='icon icon-fw icon-map-marker'
   title='Click to view coordinates'
   onClick="alert('[[+Location_lat]][[+Location_lng:prepend=`, `]][[+Location_elevation:prepend=`, `:append=` m`]]');">
</i>
`]]
[[+Location_geojson:notempty=`
<i class='icon icon-fw icon-map-o'
   title='Click to view GeoJSON data'
   onClick="alert('[[+Location_geojson:htmlent]]');">
</i>
`]]
