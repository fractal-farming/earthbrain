{
    "type": "Feature",
    "properties": {
        "name": "[[+title]]",
        "amenity": "",
        "popupContent": "[[$earthMapPopupContentEvent:strip:replace=`/==\/`? &idx=`[[+idx]]`]]",
        "id": "event-[[+id]]"
    },
    "geometry": {
        "type": "Point",
        "coordinates": [ [[+location]] ]
    }
}