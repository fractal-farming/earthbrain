{
    "type": "Feature",
    "properties": {
        "name": "[[+name:empty=`[[+title]]`]]",
        "amenity": "",
        "popupContent": "[[+popup_content]]",
        "id": "item-[[+id]]"
    },
    "geometry": {
        "type": "Point",
        "coordinates": [ [[+lng]],[[+lat]] ]
    }
}