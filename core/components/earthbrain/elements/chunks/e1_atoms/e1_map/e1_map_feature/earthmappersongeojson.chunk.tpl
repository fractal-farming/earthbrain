{
    "type": "Feature",
    "properties": {
        "name": "[[+fullname]]",
        "amenity": "",
        "popupContent": "<strong>[[+fullname]]</strong><br>[[+locality:append=`, `]][[+region]]",
        "id": "person-[[+id]]"
    },
    "geometry": {
        "type": "Point",
        "coordinates": [ [[+lng]],[[+lat]] ]
    }
}