{
    "type": "Feature",
    "properties": {
        "name": "[[+name]]",
        "amenity": "",
        "popupContent": "",
        "id": "plant-[[+id]]"
    },
    "geometry": {
        "type": "Point",
        "coordinates": [ [[+lng]],[[+lat]] ]
    }
}