[[+firstOption:after=`==[[+default]]||`]][[migxLoopCollection?
    &packageName=`[[+packageName:default=`earthbrain`]]`
    &classname=`[[+className]]`
    &tpl=`[[+rowTpl]]`
    &outputSeparator=`||`
    &where=`[ [[+where]] ]`
    &joins=`[ [[+joins]] ]`
    &sortConfig=`[{"sortby":"[[+sortBy]]","sortdir":"[[+sortDir]]"}]`
]]