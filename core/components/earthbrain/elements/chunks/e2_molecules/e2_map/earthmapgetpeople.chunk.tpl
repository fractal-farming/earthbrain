[[!getCache?
    &element=`pdoUsers`
    [[++custom_cache:eq=`1`:then=`&cacheKey=`custom/people/[[+user_access_level]]``]]

    &limit=`0`
    &offset=`0`

    &users=`[[+users]]`
    &groups=`[[+groups]]`
    &roles=`[[+roles]]`

    &showInactive=`[[+show_inactive:default=`1`]]`
    &showBlocked=`[[+show_blocked:default=`0`]]`

    &tpl=`earthMapPersonGeoJSON`
    &tplWrapper=`earthMapWrapperGeoJSON`
    &where=`[
        {"Location.lat:!=":""},
        {"Location.lng:!=":""},
        {"Data.published:=":1},
        {"Location.published:=":1}
    ]`
    &outputSeparator=`,`
    &sortby=`modUser.id`

    &leftJoin=`{
        "Data": {
            "class": "earthPersonData",
            "on": "modUser.id = Data.person_id"
        },
        "Location": {
            "class": "earthLocation",
            "on": "Data.location_id = Location.id"
        },
        "Address": {
            "class": "earthAddress",
            "on": "Data.address_id = Address.id"
        }
    }`
    &groupby=`modUser.id`
    &select=`{
        "Data": "Data.firstname AS firstname, Data.middlename AS middlename, Data.lastname AS lastname, Data.published AS published",
        "Address": "Address.id AS address, Address.locality AS locality, Address.region AS region",
        "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng, Location.radius AS radius",
        "modUser": "id,username,createdon"
    }`

    &prepareSnippet=`earthMapPreparePeople`

    &showLog=`0`
]]