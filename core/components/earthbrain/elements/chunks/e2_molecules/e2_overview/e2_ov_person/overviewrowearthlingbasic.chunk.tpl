[[-$imgOverviewLinkJSON?
    &uid=`[[+unique_idx]]`
    &classes=`rounded`
]]

<[[+level]] class="ui header">[[+[[+title_field]]]]</[[+level]]>
[[If?
    &subject=`[[+show_subtitle]]`
    &operator=`EQ`
    &operand=`1`
    &then=`<p class="subtitle meta">[[+locality:append=`, `]][[+region]]</p>`
]]

[[[[If?
    &subject=`[[+show_introtext]]`
    &operator=`EQ`
    &operand=`1`
    &then=`$introtextDescription? &uid=`[[+unique_idx]]``
]]]]

[[[[If?
    &subject=`[[+link_text]]`
    &operator=`isnot`
    &operand=`0`
    &then=`$buttonHrefOverview:prepend=`<p>`:append=`</p>`? &classes=`large primary``
]]]]