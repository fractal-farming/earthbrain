[[-$imgOverviewLinkJSON?
    &uid=`[[+unique_idx]]`
    &classes=`rounded`
]]

<div class="center aligned content title">
    <[[+level]] class="header">[[+[[+title_field]]]]</[[+level]]>
    [[If?
        &subject=`[[+show_subtitle]]`
        &operator=`EQ`
        &operand=`1`
        &then=`<p class="subtitle meta">[[+locality:append=`, `]][[+region]]</p>`
    ]]
    [[If?
        &subject=`[[+show_introtext]]`
        &operator=`EQ`
        &operand=`1`
        &then=`<div class="description">[[+introtext]]</div>`
    ]]
</div>

[[[[If?
    &subject=`[[+link_text]]`
    &operator=`isnot`
    &operand=`0`
    &then=`$buttonHrefOverview? &classes=`large primary bottom attached``
]]]]