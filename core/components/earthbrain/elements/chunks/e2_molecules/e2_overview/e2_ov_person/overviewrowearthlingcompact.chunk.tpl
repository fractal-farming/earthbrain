<div class="item">
    <[[+level]] class="ui header">[[+[[+title_field]]]]</[[+level]]>
    [[If?
        &subject=`[[+show_subtitle]]`
        &operator=`EQ`
        &operand=`1`
        &then=`<p class="subtitle meta">[[+locality:append=`, `]][[+region]]</p>`
    ]]
</div>