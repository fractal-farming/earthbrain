<td data-title="[[%earthbrain.person.fullname]]">
    [[+img_landscape:notempty=`
    <figure class="ui image">
        <img class="ui mini image" src="[[-ImagePlus? &value=`[[+image_json]]` &options=`w=200&zc=1` &type=`thumb`]]" alt="[[+pagetitle]]">
    </figure>
    `]]
    [[+[[+title_field]]]]<br>
    <em class="meta">[[+locality:append=`, `]][[+region]]</em>
</td>

<td data-title="[[%earthbrain.person.email]]">
    [[+email]]
</td>

<td data-title="[[%earthbrain.person.phone]]">
    [[+phone]]
</td>

<td data-title="[[%earthbrain.person.active]]">
    <i class="[[+active:eq=`1`:then=`checkmark`:else=``]] icon"></i>
</td>

<td data-title="[[%earthbrain.person.published]]">
    <i class="[[+published:eq=`1`:then=`checkmark`:else=``]] icon"></i>
</td>

<td data-title="">
    <a class="ui small compact disabled primary button">
        [[%earthbrain.button.edit]]
    </a>
</td>
