<thead>
<tr>
    <th>[[%earthbrain.person.fullname]]</th>
    <th>[[%earthbrain.person.email]]</th>
    <th>[[%earthbrain.person.phone]]</th>
    <th>[[%earthbrain.person.active]]</th>
    <th>[[%earthbrain.person.published]]</th>
    <th class="collapsing"></th>
</tr>
</thead>
