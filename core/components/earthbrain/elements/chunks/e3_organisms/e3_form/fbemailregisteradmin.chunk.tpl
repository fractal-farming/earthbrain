[[%earthbrain.registration.email_heading_admin]]:<br/><br/>

[[!fbFormReport:replace=`[[++earthbrain.activation_email_alt]]==[[+fb[[*id]]-email-user]]`?
    &formID=`[[*id]]`
    &tplPrefix=`fbEmailRow_`
    &tplSectionHeader=`fbEmailSectionHeader`
    &outputReverse=`1`
]]

<br/><br/><br/>

<a href="[[+confirmUrl]]">[[%earthbrain.registration.email_confirm_admin]]</a>