[[setBoxType? &input=`[[+row_tpl]]` &prefix=`pt_[[+layout_id]]_[[+unique_idx]]`]]

[[Switch:toPlaceholder=`[[+prefix]].sortdir`?
    &get=`[[+sortby]]_[[+sortdir]]`

    &c1=`createdon_0`     &do1=`DESC`
    &c2=`lastname_0`      &do2=`ASC`
    &c7=`createdon_1`     &do7=`ASC`
    &c8=`lastname_1`      &do8=`DESC`

    &default=`DESC`
]]
[[[[modifiedIf? &subject=`[[+prefix]]` &operator=`contains` &operand=`__` &then=`$overviewSettingsPrepareMgr`]]]]

<table id="[[If? &subject=`[[+prefix]]` &operator=`is` &operand=`pt__` &then=`pt_[[Time]]` &else=`[[+prefix]]`]]"
       class="ui [[+[[+prefix]].grid_settings]] [[+padding:replace=`relaxed==padded`]] overview [[+[[+prefix]].box_type]]"
    >
    [[$[[+row_tpl]]Head? &uid=`[[+prefix]]`]]
    <tbody>
    [[![[If? &subject=`[[+pagination]]` &operator=`EQ` &operand=`1` &then=`pdoPage` &else=`getCache`]]?
        &element=`pdoUsers`
        [[++custom_cache:eq=`1`:then=`&cacheKey=`custom/people/[[+user_access_level]]``]]

        &limit=`[[+limit:default=`0`]]`
        &offset=`[[+offset:default=`0`]]`
        &tpl=`overviewRowEarthling[[+[[+prefix]].row_type]]`

        &users=`[[+users]]`
        &groups=`[[+groups:empty=`[[++romanesco.member_groups_frontend]]`]]`
        &roles=`[[+roles]]`

        &showInactive=`[[+show_inactive]]`
        &showBlocked=`[[+show_blocked]]`

        &leftJoin=`{
            "Data": {
                "class": "earthPersonData",
                "on": "modUser.id = Data.person_id"
            },
            "Location": {
                "class": "earthLocation",
                "on": "Data.location_id = Location.id"
            },
            "Address": {
                "class": "earthAddress",
                "on": "Data.address_id = Address.id"
            }
        }`
        &groupby=`modUser.id`
        &select=`{
            "Data": "Data.firstname AS firstname, Data.middlename AS middlename, Data.lastname AS lastname, Data.published AS published",
            "Address": "Address.id AS address, Address.locality AS locality, Address.region AS region",
            "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng"
        }`
        &showLog=`0`

        &sortby=`[[If? &subject=`[[+users]]` &operator=`notempty` &then=`FIELD(modUser.id, [[+users]])` &else=`[[+sortby]]`]]`
        &sortdir=`[[+[[+prefix]].sortdir]]`

        [[$overviewSettings? &uid=`[[+prefix]]`]]
        [[[[+pagination:eq=`1`:then=`$overviewSettingsPagination? &uid=`[[+prefix]]``]]]]
    ]]
    </tbody>
    <tfoot class="pagination-table">
        [[+pagination:eq=`1`:then=`[[!+[[+prefix]].page.nav]]`]]
    </tfoot>
</table>

[[loadAssets? &component=`table`]]
