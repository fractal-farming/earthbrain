<?php
/**
 * migxLoadPlanting
 *
 * When parsing options inside a selector with @CHUNK, it seems impossible to
 * read existing values from other fields. This hook fetches the existing class
 * key and adds it to the request, making it available for templating.
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

// Forward class key, for use in TV templating
$classKey = $scriptProperties['record']['class_key'] ?? '';
$_POST['object_class_key'] = $classKey;

// Set member groups TV
if ($scriptProperties['record']['person_id'] ?? false) {
    $userGroupsTV = [];
    $userGroups = $modx->getCollection('modUserGroupMember', [
        'member' => $scriptProperties['record']['person_id'],
    ]);

    foreach ($userGroups as $group) {
        $userGroupsTV[] = $group->get('user_group');
    }

    if ($userGroupsTV) {
        $scriptProperties['record']['groups'] = implode('||', $userGroupsTV);
    }
}

return '';