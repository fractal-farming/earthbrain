<?php
/**
 * migxSaveExchange
 *
 * Aftersave snippet for exchanges.
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));

if (!($earthbrain instanceof EarthBrain)) return;

$object = $modx->getOption('object', $scriptProperties);
$properties = $modx->getOption('scriptProperties', $scriptProperties, []);
$configs = $modx->getOption('configs', $properties, '');
$postValues = $modx->getOption('postvalues', $scriptProperties, []);
$co_id = $modx->getOption('co_id', $properties);

//$modx->log(modX::LOG_LEVEL_ERROR, print_r($properties,1));
//$modx->log(modX::LOG_LEVEL_ERROR, $object->get('id'));

$result = [];

if (!is_object($object)) return;

if (str_contains($configs, 'exchange_items')) {
    $object->set('exchange_id', $co_id);
}
elseif (str_contains($configs, 'exchange_smeti')) {
    $q = $modx->newQuery('earthExchange',[
        'id' => $co_id,
    ]);
    $q->select('exchange_id');
    $oppositeID = $modx->getValue($q->prepare());

    $object->set('exchange_id', $oppositeID);
}

$object->save();

return json_encode($result);