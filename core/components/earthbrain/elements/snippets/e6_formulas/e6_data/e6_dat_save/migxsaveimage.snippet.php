<?php
/**
 * migxSaveImage
 *
 * Hook snippet for images. Fire on aftersave event.
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/', array('core_path' => $corePath));
$earthimage = $modx->getService('earthimage','earthImage',$corePath . 'model/earthbrain/', array('core_path' => $corePath));

if (!($earthbrain instanceof EarthBrain)) return;
if (!($earthimage instanceof earthImage)) return;

$object = $modx->getOption('object', $scriptProperties);
$properties = $modx->getOption('scriptProperties', $scriptProperties, []);
$configs = $modx->getOption('configs', $properties);
$postValues = $modx->getOption('postvalues', $scriptProperties, []);
$co_id = $modx->getOption('co_id', $properties, 0);
$result = [];

//$modx->log(modX::LOG_LEVEL_ERROR, print_r($_REQUEST, 1));
//$modx->log(modX::LOG_LEVEL_ERROR, print_r($properties, 1));
//$modx->log(modX::LOG_LEVEL_ERROR, print_r($postValues, 1));

if (!is_object($object)) return;

// Make sure null values are really null
$earthbrain->resetNULL($object, $properties);

// Attach object to parent
$object->set('parent_id', $co_id);

// If co_id is 0, then parent might be a resource
if (!$co_id && $properties['resource_id']) {
    $object->set('parent_id', $properties['resource_id']);
}

// Create image variants with different aspect ratios
$earthimage->setImageVariants($object);

// Extract Exif data from image
$imageExif = $earthimage->getExifData($object->get('img'));

// Use Exif data as fallback for manually entered values
if ($imageExif) {
    $properties['Location_lat'] = $properties['Location_lat'] ?: $imageExif['lat'];
    $properties['Location_lng'] = $properties['Location_lng'] ?: $imageExif['lng'];

    // Elevation can be 0, so avoid interpreting that as empty
    if ($properties['Location_elevation'] != 0) {
        $properties['Location_elevation'] = $properties['Location_elevation'] ?: $imageExif['elevation'];
    }

    // Attach actual date taken to image
    if ($imageExif['date'] && !$properties['date']) {
        $object->set('date', $imageExif['date']);
    }
}

// Use filename as title if not set
if (!$object->get('title') && $object->get('path')) {
    $object->set('title', $object->get('path'));
}

// Wrap up
$earthbrain->saveLocation($object, $properties);
$earthbrain->incrementPosition($object, $properties);

$object->save();

return json_encode($result);