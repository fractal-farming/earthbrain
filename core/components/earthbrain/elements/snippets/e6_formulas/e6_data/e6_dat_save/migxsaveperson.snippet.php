<?php
/**
 * migxSavePerson
 *
 * Aftersave snippet for earthPersonData object.
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
$earthlocation = $modx->getService('earthlocation','earthLocation',$corePath . 'model/earthbrain/',array('core_path' => $corePath));

if (!($earthbrain instanceof EarthBrain)) return;
if (!($earthlocation instanceof earthLocation)) return;

$earthProcessorProps = ['processors_path' => $earthbrain->config['processorsPath']];

$object = $modx->getOption('object', $scriptProperties);
$properties = $modx->getOption('scriptProperties', $scriptProperties, []);
$configs = $modx->getOption('configs', $properties, '');
$postValues = $modx->getOption('postvalues', $scriptProperties, []);
$result = [];

$objectID = $properties['object_id'] ?? null;
$personID = $properties['person_id'] ?? null;
$groups = $properties['groups'] ?? null;
$className = 'earthPersonData';

if (!is_object($object) || !$objectID) return;

// Reuse timestamp so all dates match
$createdOn = time();

// Set default ownership
$createdBy = (int)$modx->user->get('id');

// Set user groups
$userGroups = [];
if ($groups && $personID) {
    if (!is_array($groups)) {
        $groups = explode(',', $groups);
    }
    foreach ($groups as $groupID) {
        $userGroups[] = [
            "usergroup" => $groupID,
            "role" => 1,
        ];
    }
}

// Geocode address
if ($properties['Location_geocode'] ?? false) {
    $location = $earthlocation->geocodeAddress($properties['Location_geocode']);

    // Abort on error
    if (!$location || $location['error']) {
        return json_encode($location);
    }

    // Update coordinates and address, or just the coordinates
    if ($properties['Location_update_address'] ?? false) {
        $properties = array_merge($properties, $location);
    } else {
        $properties['Location_lat'] = $location['lat'] ?? null;
        $properties['Location_lng'] = $location['lng'] ?? null;
    }
}
elseif ($properties['Location_geocode_reverse'] ?? false) {
    $coordinates = explode(',', $properties['Location_geocode_reverse']);
    $location = $earthlocation->geocodeAddressReverse((float)$coordinates[0], (float)$coordinates[1]);
    $properties = array_merge($properties, $location);
}

// Collect data in arrays
$personData = [
    'object_id' => $object->get('id'), // Differs from properties ID with new object!
    'person_id' => $personID,
    'classname' => $className,
    'firstname' => $properties['firstname'] ?? '',
    'middlename' => $properties['middlename'] ?? '',
    'lastname' => $properties['lastname'] ?? '',
    'username' => $properties['Person_username'] ?? '',
    'fullname' => $properties['UserData_fullname'] ?? '',
    'address' => '',
    'country' => $properties['UserData_country'] ?? '',
    'gender' => $properties['UserData_gender'] ?? '',
    'dob' => $properties['UserData_dob'] ?? '',
    'email' => $properties['UserData_email'] ?? '',
    'phone' => $properties['UserData_phone'] ?? '',
    'mobilephone' => $properties['UserData_mobilephone'] ?? '',
    'website' => '',
    'createdon' => $createdOn,
    'class_key' => $properties['Person_class_key'] ?? '',
    'active' => $properties['Person_active'] ?? 0,
    'published' => $properties['published'] ?? 0,
    'groups' => $userGroups
];
$personAddress = [
    'line_1' => $properties['Address_line_1'] ?? '',
    'line_2' => $properties['Address_line_2'] ?? '',
    'line_3' => $properties['Address_line_3'] ?? '',
    'locality' => $properties['Address_locality'] ?? '',
    'region' => $properties['Address_region'] ?? '',
    'country' => $properties['Address_country'] ?? '',
    'postal_code' => $properties['Address_postal_code'] ?? '',
    'comments' => $properties['Address_comments'] ?? '',
    'createdon' => $createdOn,
    'createdby' => $createdBy,
    'published' => $properties['Address_published'] ?? 0,
];
$personLocation = [
    'lat' => $properties['Location_lat'] ?? null,
    'lng' => $properties['Location_lng'] ?? null,
    'elevation' => $properties['Location_elevation'] ?? null,
    'radius' => $properties['Location_radius'] ?? 0,
    'geojson' => $properties['Location_geojson'] ?? null,
    'createdon' => $createdOn,
    'createdby' => $createdBy,
    'published' => $properties['Location_published'] ?? 0,
];

// Consolidate data in single array for processor
$personData['personAddress'] = $personAddress;
$personData['personLocation'] = $personLocation;

// Create or update person
if ($objectID === 'new') {
    $response = $modx->runProcessor('data/person/create', $personData, $earthProcessorProps);
    $person = $response->getObject();
} else {
    $personData['id'] = $personID;
    $response = $modx->runProcessor('data/person/update', $personData, $earthProcessorProps);
}
if ($response->isError()) {
    $modx->log(modX::LOG_LEVEL_ERROR, print_r($response->getAllErrors(),1));
    return json_encode($response->getMessage());
}

// Reset potentially altered null fields
$earthbrain->resetNull($object, $properties);

return json_encode($result);