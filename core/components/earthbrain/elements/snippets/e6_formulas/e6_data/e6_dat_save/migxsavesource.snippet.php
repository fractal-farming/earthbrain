<?php
/**
 * migxSaveSource
 *
 * Aftersave snippet for sources.
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
$earthlocation = $modx->getService('earthlocation','earthLocation',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
$earthimage = $modx->getService('earthimage','earthImage',$corePath . 'model/earthbrain/',array('core_path' => $corePath));

if (!($earthbrain instanceof EarthBrain)) return;
if (!($earthlocation instanceof earthLocation)) return;
if (!($earthimage instanceof earthImage)) return;

$object = $modx->getOption('object', $scriptProperties);
$properties = $modx->getOption('scriptProperties', $scriptProperties, []);
$configs = $modx->getOption('configs', $properties, '');
$postValues = $modx->getOption('postvalues', $scriptProperties, []);
$result = [];

if (!is_object($object)) return;

$earthbrain->resetNull($object, $properties);

// Geocode addresses
if ($properties['Location_geocode'] ?? false) {
    $location = $earthlocation->geocodeAddress($properties['Location_geocode']);

    // Abort on error
    if (!$location || $location['error']) {
        return json_encode($location);
    }

    // Update coordinates and address, or just the coordinates
    if ($properties['Location_update_address'] ?? false) {
        $properties = array_merge($properties, $location);
    } else {
        $properties['Location_lat'] = $location['lat'] ?? null;
        $properties['Location_lng'] = $location['lng'] ?? null;
    }
}
elseif ($properties['Location_geocode_reverse'] ?? false) {
    $location = $earthlocation->geocodeAddressReverse($properties['Location_geocode_reverse']);
    $properties = array_merge($properties, $location);
}

// Attempt to extract location from image
if ($properties['Location_from_image'] ?? false) {
    $path = $properties['Location_from_image'];
    $source = $modx->getOption('earthbrain.img_source_meta');

    if ($location = $earthimage->getExifData($path, $source)) {
        $properties['Location_lat'] = $location['lat'] ?? null;
        $properties['Location_lng'] = $location['lng'] ?? null;
        $properties['Location_elevation'] = $location['elevation'] ?? null;
    }
}

$earthbrain->saveAddress($object, $properties);
$earthbrain->saveLocation($object, $properties);

return json_encode($result);