<?php
/**
 * migxWhere
 *
 * Snippet to generate a where clause for populating MIGX grids.
 *
 * Conditions differ per class type, but objects are excluded by default if they
 * are not created by the logged in user.
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/', array('core_path' => $corePath));

// Get parent config from stored request parameters
$objectID = $modx->getOption('object_id', $_REQUEST);
$parentConfig = $modx->getOption('reqConfigs', $_REQUEST);
$configs = $modx->getOption('configs', $scriptProperties, '');
$classType = $modx->getOption('type', $scriptProperties);

$classKeys = [];
$where = [];

// Limit listings to objects created by user (for all non-admins)
if (!$modx->user->sudo) {
    $where['createdby'] = $modx->user->id;
}

// List people in same user group(s) (for Editors)
if ($classType === 'person' && !$modx->user->sudo) {
    unset($where['createdby']);

    // Always allow editing yourself
    $userList = [$modx->user->id];

    // Get all groups that this user is a member of
    $userGroups = $modx->getCollection('modUserGroup', [
        'id:IN' => $modx->user->getUserGroups()
    ]);

    // Find out if user has at least Editor role inside each group
    foreach ($userGroups as $userGroup) {
        // Connect with your higher self
        $self = $modx->getObject('modUserGroupMember', [
            'user_group' => $userGroup->get('id'),
            'member' => $modx->user->id,
        ]);

        // Find your inner purpose
        if ($self) {
            $selfRole = $self->getOne('UserGroupRole')->authority;
        } else {
            continue; // go in peace, bodhisattva
        }

        // Add group members to list for Editors and above
        if ($selfRole <= 10) {
            $userGroupMembers = $userGroup->getMany('UserGroupMembers');
            foreach ($userGroupMembers as $userGroupMember) {
                $user = $userGroupMember->getOne('User');
                // ...but skip if outranked
                if ($selfRole > $userGroupMember->getOne('UserGroupRole')->authority) {
                    continue;
                }
                $userList[] = $user->get('id');
            }
        }
    }

    $where['person_id:IN'] = array_unique($userList);
}

// Allow everyone to edit links inside Taxonomy and Source grids
if ($classType === 'link' && str_contains($parentConfig, 'earthbrain_taxonomy')) {
    unset($where['createdby']);
}
if ($classType === 'link' && str_contains($parentConfig, 'earthbrain_sources')) {
    unset($where['createdby']);
}

// Allow everyone to edit public sources
if ($classType === 'source') {
    $where['OR:published:='] = 1;
}

// Connect plantings to parent object
// (elseif from here!!)
elseif ($classType === 'planting') {
    $where['plant_id'] = $objectID;
}

// Connect exchange items to parent object
elseif ($classType === 'exchange_item') {
    unset($where['createdby']);
    $where['exchange_id'] = $objectID;
}

// Link exchange items in opposite direction
elseif ($classType === 'exchange_meti') {
    unset($where['createdby']);
    $q = $modx->newQuery('earthExchange',[
        'id' => $objectID,
    ]);
    $q->select('exchange_id');
    $oppositeID = $modx->getValue($q->prepare());

    $where['exchange_id'] = $oppositeID;
}

// Link extended objects to parent_id
elseif ($objectID && $parentConfig) {
    $migxProperties = $earthbrain->getMigxProperties($parentConfig);

    $where['class_key'] = $migxProperties['classKeys'][$classType] ?? '';
    $where['parent_id'] = $objectID;
}

$where = json_encode($where);
$validate = $earthbrain->validateJSON($where);

if (!$validate) {
    return '';
}

return $where;