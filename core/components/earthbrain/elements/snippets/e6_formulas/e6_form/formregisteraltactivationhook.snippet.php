<?php
/**
 * formRegisterAltActivationHook snippet
 *
 * Set alternative email address for receiving the activation email.
 * Watch out for subsequent hooks failing. This could lead to the original email
 * value getting lost after resubmitting the form.
 *
 * @var modX $modx
 * @var object $hook
 * @var array $scriptProperties
 */

$formID = $modx->resource->get('id');
$formPrefix = 'fb' . $formID . '-';
$formData = [];

// Remove prefix from field names and trim values
foreach ($hook->getValues() as $key => $value) {
    $key = str_replace($formPrefix,'',$key);
    if (is_string($value)) $value = trim($value);
    $formData[$key] = $value;
}

// Divert activation email to alternative address
$emailAlt = $modx->getOption('earthbrain.activation_email_alt', $scriptProperties);
if (!isset($formData['email-user']) && $formData['email'] != $emailAlt) {
    $hook->setValue($formPrefix . 'email', $emailAlt);

    // Store registrant email in temporary field
    $hook->setValue($formPrefix . 'email-user', $formData['email']);
}

return true;