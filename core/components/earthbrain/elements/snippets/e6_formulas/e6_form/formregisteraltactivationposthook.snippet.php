<?php
/**
 * formRegisterAltActivationPostHook snippet
 *
 * Send form submission to an alternative email address (other than the
 * registrants address). This can contain the activation link, which allows for
 * someone else (admin / moderator) to check and confirm activation requests.
 *
 * @var modX $modx
 * @var object $hook
 * @var array $scriptProperties
 */

$properties = $hook->login->controller->config;
$formID = $modx->resource->get('id');
$formPrefix = 'fb' . $formID . '-';
$formData = [];

// Remove prefix from field names and trim values
foreach ($hook->getValues() as $key => $value) {
    $key = str_replace($formPrefix,'',$key);
    if (is_string($value)) $value = trim($value);
    $formData[$key] = $value;
}

// Reference to modUser and modUserProfile objects
$user = $hook->getValue('register.user');
$profile = $hook->getValue('register.profile');

// Send email to registrant
$emailFrom = $modx->getOption('emailsender', $scriptProperties);
$emailFromName = $modx->getOption('site_name', $scriptProperties);
$subject = $modx->lexicon('earthbrain.registration.email_subject', ['site_name' => $emailFromName]);
$message = $modx->getChunk('fbEmailRegisterAlt', $hook->getValues());

$modx->getService('mail', 'mail.modPHPMailer');
$modx->mail->set(modMail::MAIL_BODY, $message);
$modx->mail->set(modMail::MAIL_FROM, $emailFrom);
$modx->mail->set(modMail::MAIL_FROM_NAME, $emailFromName);
$modx->mail->set(modMail::MAIL_SUBJECT, $subject);
$modx->mail->address('to', $formData['email-user']);
$modx->mail->address('reply-to', $emailFrom);
$modx->mail->setHTML(true);
if (!$modx->mail->send()) {
    $modx->log(modX::LOG_LEVEL_ERROR,'An error occurred while trying to send the email: '.$modx->mail->mailer->ErrorInfo);
}
$modx->mail->reset();

// Revert to original user email address
$profile->set('email', $formData['email-user']);
$profile->save();

return true;