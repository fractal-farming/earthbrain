<?php
/**
 * formRegisterSavePerson hook
 *
 * Create extended EarthPerson object.
 * Keep in mind that the user is already created at this point.
 *
 * @var modX $modx
 * @var object $hook
 * @var array $scriptProperties
 */

$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
$corePath = $modx->getOption('romanescobackyard.core_path', null, $modx->getOption('core_path') . 'components/romanescobackyard/');
$romanesco = $modx->getService('romanesco','Romanesco',$corePath . 'model/romanescobackyard/',array('core_path' => $corePath));

if (!($earthbrain instanceof EarthBrain)) return;
if (!($romanesco instanceof Romanesco)) return;

$earthProcessorProps = ['processors_path' => $earthbrain->config['processorsPath']];

$formID = $modx->resource->get('id');
$formPrefix = 'fb' . $formID . '-';
$formData = [];

// Remove prefix from field names and trim values
foreach ($hook->getValues() as $key => $value) {
    $key = str_replace($formPrefix,'',$key);
    if (is_string($value)) $value = trim($value);
    $formData[$key] = $value;
}

// Reuse timestamp so all dates match
$createdOn = time();

// A reference to the modUser object
$user = $hook->getValue('register.user');
// A reference to the modUserProfile object
$profile = $hook->getValue('register.profile');

// Geocode addresses
$location = [];
if ($formData['address'] ?? false) {
    $location = $modx->runSnippet('geocodeAddress', ['address' => $formData['address']]);
    $streetNumber = $location['properties']['streetNumber'] ?? '';
    $streetName = $location['properties']['streetName'] ?? '';
}

// Data arrays
$personData = [
    'id' => $user->get('id'), // for updating modUser
    'person_id' => $user->get('id'), // for extended object
    'firstname' => $formData['firstname'],
    'middlename' => $formData['middlename'] ?? '',
    'lastname' => $formData['lastname'],
    'fullname' => $formData['fullname'] ?? $formData['firstname'] . ' ' . $formData['lastname'],
    'username' => $formData['username'] ?? '',
    'address' => $formData['address'] ?? '',
    'country' => $location['properties']['countryCode'] ?? '',
    'email' => $formData['email-user'] ?? '',
    'phone' => $formData['phone'] ?? '',
    'website' => '',
    'createdon' => $createdOn,
    'class_key' => 'earthPerson',
    'active' => 0,
];
$personAddress = [
    'line_1' => trim("$streetNumber $streetName"),
    'line_2' => '',
    'line_3' => $location['properties']['subLocality'] ?? '',
    'locality' => $location['properties']['locality'] ?? '',
    'region' => $location['properties']['adminLevels'][1]['name'] ?? '',
    'country' => $location['properties']['countryCode'] ?? '',
    'postal_code' => $location['properties']['postalCode'] ?? '',
    'comments' => $formData['address'] ?? '',
    'createdon' => $createdOn,
    'createdby' => '',
    'published' => 0,
];
$personLocation = [
    'lat' => $location['geometry']['coordinates'][1] ?? null,
    'lng' => $location['geometry']['coordinates'][0] ?? null,
    'elevation' => null,
    'radius' => '',
    'geojson' => null,
    'createdon' => $createdOn,
    'createdby' => '',
    'published' => 0,
];

// Create single array for processor
$personData['personAddress'] = $personAddress;
$personData['personLocation'] = $personLocation;

// Create and then update extended user profile
// This bit of trickery is needed to avoid a catch 22 with the create/update
//  processors. The update processor assumes the extended data is already there,
//  and the create processor would attempt to create a user, which in this case
//  is also already there. So neither process will run, unless we create the
//  extended object first. Then the update processor can do its thing.
$earthPersonData = $modx->newObject('earthPersonData', $personData);
$earthPersonData->save();

// Update processor refers to extended object via object_id
$personData['object_id'] = $earthPersonData->get('id');

// Make sure user is earthPerson
$user->set('class_key', 'earthPerson');
$user->save();

// Run update processor
$response = $modx->runProcessor('data/person/update', $personData, $earthProcessorProps);
if ($response->isError()) {
    $modx->log(modX::LOG_LEVEL_ERROR, print_r($response->getAllErrors(),1));
    return false;
}

return true;