<?php
/**
 * earthMapPreparePeople
 *
 * Modify field values before the pdoResources snippet in earthMapGetPeople is
 * executed.
 *
 * Please note that the final output is a GeoJSON object, so each field needs to
 * generate valid JSON.
 *
 * @var modX $modx
 * @var array $scriptProperties
 * @var array $row
 */

$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/', array('core_path' => $corePath));
$earthlocation = $modx->getService('earthlocation','earthLocation',$corePath . 'model/earthbrain/', array('core_path' => $corePath));
$corePath = $modx->getOption('romanescobackyard.core_path', null, $modx->getOption('core_path') . 'components/romanescobackyard/');
$romanesco = $modx->getService('romanesco','Romanesco',$corePath . 'model/romanescobackyard/',array('core_path' => $corePath));

if (!($earthbrain instanceof EarthBrain)) return;
if (!($earthlocation instanceof earthLocation)) return;
if (!($romanesco instanceof Romanesco)) return;

$tplPopupContent = $modx->getOption('tplPopupContent', $scriptProperties, '');

// Coordinates
// =============================================================================

// Randomly generate nearby coordinates to obfuscate real location
$lat = $row['lat'] ?? null;
$lng = $row['lng'] ?? null;
$radius = $row['radius'] ?? 0;

if ($lat && $lng && $radius > 0) {
    $randomLocation = array();
    $randomLocation = $earthlocation->obfuscateCoordinates(array($lat,$lng), $radius);

    $row['lat'] = $randomLocation['lat'];
    $row['lng'] = $randomLocation['lng'];

    $row['message'] = $modx->getChunk('richTextMessage', array(
        'size' => 'small',
        'message_type' => 'info',
        'heading' => $modx->lexicon('earthbrain.map.location_obfuscated_heading'),
        'content' => $modx->lexicon('earthbrain.map.location_obfuscated_content', array('radius'=>$row['radius'])),
    ));
}

return json_encode($row);