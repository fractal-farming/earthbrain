<?php
/**
 * @var modX $modx
 * @var string $input
 */

switch($input) {
    case stripos($input,'PersonBasicCard') !== false:
        $box_type = "cards";
        $row_type = "";
        $column_type = "card";
        $grid_settings = "";
        break;
    case stripos($input,'PersonWideSegment') !== false:
        $box_type = "basic segments";
        $row_type = "";
        $column_type = "ui [[+padding:replace=`relaxed==padded`]] segment";
        $grid_settings = "[[+de_emphasize:ne=`1`:then=`raised`]]";
        break;
    case stripos($input,'PersonTable') !== false:
        $box_type = "sortable table";
        $row_type = "table";
        $column_type = "";
        $grid_settings = "small very compact [[+de_emphasize:eq=`1`:then=`very basic`]]";
        break;
    case stripos($input,'PersonBasic') !== false:
        $box_type = "grid";
        $row_type = "";
        $column_type = "column";
        $grid_settings = "column";
        break;
    default:
        return false;
}

return [
    'box_type' => $box_type,
    'row_type' => $row_type,
    'column_type' => $column_type,
    'grid_settings' => $grid_settings,
];