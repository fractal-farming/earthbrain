<?php

// Map
// =====================================================================

$_lang['earthbrain.map.button_read_more'] = "More info";
$_lang['earthbrain.map.location_obfuscated_heading'] = "This is not the exact location.";
$_lang['earthbrain.map.location_obfuscated_content'] = "For privacy / security reasons, the marker has been placed randomly within a [[+radius]] km radius.";

// Location
// =====================================================================

$_lang['earthbrain.location.heading'] = "Location";
$_lang['earthbrain.location.elevation'] = "Elevation";

// Source
// =====================================================================

$_lang['earthbrain.source.heading'] = "Source";
$_lang['earthbrain.source.unknown'] = "Unknown / Nature";

// Address
// =====================================================================

$_lang['earthbrain.address.heading'] = "Address";

// Person
// =====================================================================

$_lang['earthbrain.person.fullname'] = "Name";
$_lang['earthbrain.person.firstname'] = "First name";
$_lang['earthbrain.person.middlename'] = "Middle name";
$_lang['earthbrain.person.lastname'] = "Last name";
$_lang['earthbrain.person.username'] = "Nickname";
$_lang['earthbrain.person.email'] = "Email";
$_lang['earthbrain.person.phone'] = "Phone";
$_lang['earthbrain.person.dob'] = "Birthdate";
$_lang['earthbrain.person.gender'] = "Gender";
$_lang['earthbrain.person.address'] = "Address";
$_lang['earthbrain.person.image'] = "Image";
$_lang['earthbrain.person.active'] = "Active";
$_lang['earthbrain.person.published'] = "Public";

// User interface
// =====================================================================

$_lang['earthbrain.button.edit'] = "Edit";

// Registration
// =====================================================================

$_lang['earthbrain.registration.email_subject'] = "Your [[+site_name]] registration";
$_lang['earthbrain.registration.email_heading_admin'] = "New registration";
$_lang['earthbrain.registration.email_confirm_admin'] = "Confirm this registration";
$_lang['earthbrain.registration.email_confirm_person'] = "Confirm my registration";
$_lang['earthbrain.registration.email_content'] = '
<p>Dear [[+fb[[*id]]-firstname]],</p>
<p>Thank you for registering as a [[++site_name]] member.</p>
<p>Please wait for the Membership Committee to approve your registration.</p>
<p>Sincerely,<br><em>[[++site_name]]</em></p>
';