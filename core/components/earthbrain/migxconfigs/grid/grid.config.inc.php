<?php

$gridcontextmenus['copylocation']['code']="
    m.push({
        className : 'copylocation',
        text: 'Copy location',
        handler: 'this.copylocation'
    });
    m.push('-');
";
$gridcontextmenus['copylocation']['handler'] = 'this.copylocation';

$gridfunctions['this.copylocation'] = "
copylocation: function(btn,e) {
    var _this=this;
    Ext.Msg.confirm(_('') || '','Bist du sicher?',function(e) {
        if (e == 'yes') {
            MODx.Ajax.request({
                url: _this.config.url
                ,params: {
                    action: 'mgr/migxdb/update'
                    ,processaction: ''
                    ,configs: _this.config.configs
                    ,resource_id: _this.config.resource_id
                    ,co_id: '[[+config.connected_object_id]]'
                    ,reqConfigs: '[[+config.req_configs]]'
                }
                ,listeners: {
                    'success': {fn:function(r) {
                        _this.refresh();
                    },scope:_this}
                }
            });
        }
    }),this;           
    return true;
}
";