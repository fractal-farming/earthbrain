<?php
/**
 * @package EarthBrain
 */

// Autoloader is not yet available during install
if (!class_exists('EarthBrain', false)) {
    require_once dirname(__DIR__, 2) . '/src/EarthLocation.php';
}

class earthLocation extends \FractalFarming\EarthBrain\EarthLocation
{}