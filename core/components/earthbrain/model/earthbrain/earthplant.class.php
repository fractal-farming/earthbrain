<?php
/**
 * @package EarthBrain
 */

// Autoloader is not yet available during install
if (!class_exists('EarthBrain', false)) {
    require_once dirname(__DIR__, 2) . '/src/EarthPlant.php';
}

class earthPlant extends \FractalFarming\EarthBrain\EarthPlant
{}