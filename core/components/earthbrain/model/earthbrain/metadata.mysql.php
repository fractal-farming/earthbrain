<?php

$xpdo_meta_map = array (
  'xPDOSimpleObject' => 
  array (
    0 => 'earthTaxon',
    1 => 'earthSeed',
    2 => 'earthPlant',
    3 => 'earthPlanting',
    4 => 'earthPersonData',
    5 => 'earthSource',
    6 => 'earthExchange',
    7 => 'earthExchangeItem',
    8 => 'earthAddress',
    9 => 'earthLocation',
    10 => 'earthImage',
    11 => 'earthNote',
    12 => 'earthLink',
  ),
  'xPDOObject' => 
  array (
    0 => 'earthPlantingFeature',
    1 => 'earthImageTag',
  ),
  'modUser' => 
  array (
    0 => 'earthPerson',
  ),
  'earthSource' => 
  array (
    0 => 'earthSourcePerson',
    1 => 'earthSourceEvent',
    2 => 'earthSourceOnline',
    3 => 'earthSourceStore',
    4 => 'earthSourceMarket',
    5 => 'earthSourceCoop',
    6 => 'earthSourceGov',
    7 => 'earthSourceOrg',
    8 => 'earthSourceUnknown',
  ),
  'earthExchange' => 
  array (
    0 => 'earthExchangePersonPerson',
    1 => 'earthExchangePersonSource',
    2 => 'earthExchangeSourcePerson',
  ),
  'earthExchangeItem' => 
  array (
    0 => 'earthExchangeItemSeed',
    1 => 'earthExchangeItemPlant',
    2 => 'earthExchangeItemMoney',
    3 => 'earthExchangeItemProduct',
    4 => 'earthExchangeItemService',
  ),
  'earthImage' => 
  array (
    0 => 'earthImageSeed',
    1 => 'earthImagePlant',
    2 => 'earthImagePlanting',
    3 => 'earthImageSource',
    4 => 'earthImageExchange',
  ),
  'earthNote' => 
  array (
    0 => 'earthNoteTaxon',
    1 => 'earthNoteSeed',
    2 => 'earthNotePlant',
    3 => 'earthNotePlanting',
    4 => 'earthNoteSource',
    5 => 'earthNoteExchange',
  ),
  'earthLink' => 
  array (
    0 => 'earthLinkTaxon',
    1 => 'earthLinkSource',
  ),
  'rmOption' => 
  array (
    0 => 'earthOption',
  ),
);