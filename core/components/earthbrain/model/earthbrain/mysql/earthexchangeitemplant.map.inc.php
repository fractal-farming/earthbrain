<?php
/**
 * @package EarthBrain
 */
$xpdo_meta_map['earthExchangeItemPlant']= array (
  'package' => 'earthbrain',
  'version' => '1.1',
  'extends' => 'earthExchangeItem',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'aggregates' => 
  array (
    'Plant' => 
    array (
      'class' => 'earthPlanting',
      'local' => 'parent_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
