<?php
/**
 * @package EarthBrain
 */
$xpdo_meta_map['earthExchangeItemSeed']= array (
  'package' => 'earthbrain',
  'version' => '1.1',
  'extends' => 'earthExchangeItem',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'aggregates' => 
  array (
    'Seed' => 
    array (
      'class' => 'earthSeed',
      'local' => 'parent_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
