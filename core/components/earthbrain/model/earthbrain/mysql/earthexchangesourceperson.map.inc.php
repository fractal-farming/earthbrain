<?php
/**
 * @package EarthBrain
 */
$xpdo_meta_map['earthExchangeSourcePerson']= array (
  'package' => 'earthbrain',
  'version' => '1.1',
  'extends' => 'earthExchange',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'aggregates' => 
  array (
    'Source' => 
    array (
      'class' => 'earthSource',
      'local' => 'parent_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'TargetPerson' => 
    array (
      'class' => 'earthPerson',
      'local' => 'target_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'TargetPersonData' => 
    array (
      'class' => 'earthPersonData',
      'local' => 'target_id',
      'foreign' => 'person_id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'TargetUserData' => 
    array (
      'class' => 'modUserProfile',
      'local' => 'target_id',
      'foreign' => 'internalKey',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
