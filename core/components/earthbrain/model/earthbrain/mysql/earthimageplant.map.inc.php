<?php
/**
 * @package EarthBrain
 */
$xpdo_meta_map['earthImagePlant']= array (
  'package' => 'earthbrain',
  'version' => '1.1',
  'extends' => 'earthImage',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'aggregates' => 
  array (
    'Plant' => 
    array (
      'class' => 'earthPlant',
      'local' => 'parent_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
