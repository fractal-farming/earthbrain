<?php
/**
 * @package EarthBrain
 */
$xpdo_meta_map['earthImageTag']= array (
  'package' => 'earthbrain',
  'version' => '1.1',
  'table' => 'earthbrain_image_tags',
  'extends' => 'xPDOObject',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
    'image_id' => 0,
    'option_id' => 0,
  ),
  'fieldMeta' => 
  array (
    'image_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
    ),
    'option_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
    ),
  ),
  'indexes' => 
  array (
    'PRIMARY' => 
    array (
      'alias' => 'PRIMARY',
      'primary' => true,
      'unique' => true,
      'columns' => 
      array (
        'image_id' => 
        array (
          'collation' => 'A',
          'null' => false,
        ),
        'option_id' => 
        array (
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
  ),
  'aggregates' => 
  array (
    'Image' => 
    array (
      'class' => 'earthImage',
      'local' => 'image_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'Tag' => 
    array (
      'class' => 'earthOption',
      'local' => 'option_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
