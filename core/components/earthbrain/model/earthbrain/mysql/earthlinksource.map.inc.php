<?php
/**
 * @package EarthBrain
 */
$xpdo_meta_map['earthLinkSource']= array (
  'package' => 'earthbrain',
  'version' => '1.1',
  'extends' => 'earthLink',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'aggregates' => 
  array (
    'Source' => 
    array (
      'class' => 'earthSource',
      'local' => 'parent_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
