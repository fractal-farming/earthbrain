<?php
/**
 * @package EarthBrain
 */
$xpdo_meta_map['earthLocation']= array (
  'package' => 'earthbrain',
  'version' => '1.1',
  'table' => 'earthbrain_locations',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
    'lat' => NULL,
    'lng' => NULL,
    'elevation' => NULL,
    'radius' => 0,
    'zoom' => NULL,
    'geojson' => '',
    'kml' => '',
    'createdon' => 0,
    'createdby' => 0,
    'editedon' => 0,
    'editedby' => 0,
    'published' => 0,
    'deleted' => 0,
  ),
  'fieldMeta' => 
  array (
    'lat' => 
    array (
      'dbtype' => 'decimal',
      'precision' => '8,6',
      'phptype' => 'float',
      'null' => true,
    ),
    'lng' => 
    array (
      'dbtype' => 'decimal',
      'precision' => '9,6',
      'phptype' => 'float',
      'null' => true,
    ),
    'elevation' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'phptype' => 'integer',
      'null' => true,
    ),
    'radius' => 
    array (
      'dbtype' => 'int',
      'precision' => '3',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
    ),
    'zoom' => 
    array (
      'dbtype' => 'int',
      'precision' => '3',
      'phptype' => 'integer',
      'null' => true,
    ),
    'geojson' => 
    array (
      'dbtype' => 'mediumtext',
      'phptype' => 'json',
      'null' => false,
      'default' => '',
    ),
    'kml' => 
    array (
      'dbtype' => 'mediumtext',
      'phptype' => 'string',
      'null' => false,
      'default' => '',
    ),
    'createdon' => 
    array (
      'dbtype' => 'int',
      'precision' => '20',
      'phptype' => 'timestamp',
      'null' => false,
      'default' => 0,
    ),
    'createdby' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
    ),
    'editedon' => 
    array (
      'dbtype' => 'int',
      'precision' => '20',
      'phptype' => 'timestamp',
      'null' => false,
      'default' => 0,
    ),
    'editedby' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
    ),
    'published' => 
    array (
      'dbtype' => 'tinyint',
      'precision' => '1',
      'attributes' => 'unsigned',
      'phptype' => 'boolean',
      'null' => false,
      'default' => 0,
    ),
    'deleted' => 
    array (
      'dbtype' => 'tinyint',
      'precision' => '1',
      'attributes' => 'unsigned',
      'phptype' => 'boolean',
      'null' => false,
      'default' => 0,
    ),
  ),
  'indexes' => 
  array (
    'lat' => 
    array (
      'alias' => 'lat',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'lat' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => true,
        ),
      ),
    ),
    'lng' => 
    array (
      'alias' => 'lng',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'lng' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => true,
        ),
      ),
    ),
    'elevation' => 
    array (
      'alias' => 'elevation',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'elevation' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => true,
        ),
      ),
    ),
    'createdby' => 
    array (
      'alias' => 'createdby',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'createdby' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
    'published' => 
    array (
      'alias' => 'published',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'published' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
  ),
  'aggregates' => 
  array (
    'Seed' => 
    array (
      'class' => 'earthSeed',
      'local' => 'id',
      'foreign' => 'location_id',
      'cardinality' => 'one',
      'owner' => 'local',
    ),
    'Plant' => 
    array (
      'class' => 'earthPlanting',
      'local' => 'id',
      'foreign' => 'location_id',
      'cardinality' => 'one',
      'owner' => 'local',
    ),
    'Person' => 
    array (
      'class' => 'earthPerson',
      'local' => 'id',
      'foreign' => 'location_id',
      'cardinality' => 'one',
      'owner' => 'local',
    ),
    'Source' => 
    array (
      'class' => 'earthSource',
      'local' => 'id',
      'foreign' => 'location_id',
      'cardinality' => 'one',
      'owner' => 'local',
    ),
    'Image' => 
    array (
      'class' => 'earthImage',
      'local' => 'id',
      'foreign' => 'location_id',
      'cardinality' => 'one',
      'owner' => 'local',
    ),
    'Forest' => 
    array (
      'class' => 'forestData',
      'local' => 'id',
      'foreign' => 'location_id',
      'cardinality' => 'one',
      'owner' => 'local',
    ),
    'ForestFeature' => 
    array (
      'class' => 'forestFeature',
      'local' => 'id',
      'foreign' => 'location_id',
      'cardinality' => 'one',
      'owner' => 'local',
    ),
    'ForestZone' => 
    array (
      'class' => 'forestZone',
      'local' => 'id',
      'foreign' => 'location_id',
      'cardinality' => 'one',
      'owner' => 'local',
    ),
    'ForestComponent' => 
    array (
      'class' => 'forestComponent',
      'local' => 'id',
      'foreign' => 'location_id',
      'cardinality' => 'one',
      'owner' => 'local',
    ),
    'CreatedBy' => 
    array (
      'class' => 'modUser',
      'local' => 'createdby',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'EditedBy' => 
    array (
      'class' => 'modUser',
      'local' => 'editedby',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
