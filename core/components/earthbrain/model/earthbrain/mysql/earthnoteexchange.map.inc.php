<?php
/**
 * @package EarthBrain
 */
$xpdo_meta_map['earthNoteExchange']= array (
  'package' => 'earthbrain',
  'version' => '1.1',
  'extends' => 'earthNote',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'aggregates' => 
  array (
    'Exchange' => 
    array (
      'class' => 'earthExchange',
      'local' => 'parent_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
