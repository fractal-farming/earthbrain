<?php
/**
 * @package EarthBrain
 */
$xpdo_meta_map['earthNoteTaxon']= array (
  'package' => 'earthbrain',
  'version' => '1.1',
  'extends' => 'earthNote',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'aggregates' => 
  array (
    'Taxon' => 
    array (
      'class' => 'earthTaxon',
      'local' => 'parent_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
