<?php
/**
 * @package EarthBrain
 */
$xpdo_meta_map['earthOption']= array (
  'package' => 'earthbrain',
  'version' => '1.1',
  'extends' => 'rmOption',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'composites' => 
  array (
    'Plantings' => 
    array (
      'class' => 'earthPlantingFeature',
      'local' => 'id',
      'foreign' => 'option_id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
    'Images' => 
    array (
      'class' => 'earthImageTag',
      'local' => 'id',
      'foreign' => 'image_id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
  ),
);
