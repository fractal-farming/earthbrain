<?php
/**
 * @package EarthBrain
 */
$xpdo_meta_map['earthPerson']= array (
  'package' => 'earthbrain',
  'version' => '1.1',
  'extends' => 'modUser',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'composites' => 
  array (
    'PersonData' => 
    array (
      'class' => 'earthPersonData',
      'local' => 'id',
      'foreign' => 'person_id',
      'cardinality' => 'one',
      'owner' => 'local',
    ),
  ),
);
