<?php
/**
 * @package EarthBrain
 */
$xpdo_meta_map['earthPlantingFeature']= array (
  'package' => 'earthbrain',
  'version' => '1.1',
  'table' => 'earthbrain_planting_features',
  'extends' => 'xPDOObject',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
    'planting_id' => 0,
    'option_id' => 0,
  ),
  'fieldMeta' => 
  array (
    'planting_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
    ),
    'option_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
    ),
  ),
  'indexes' => 
  array (
    'PRIMARY' => 
    array (
      'alias' => 'PRIMARY',
      'primary' => true,
      'unique' => true,
      'columns' => 
      array (
        'planting_id' => 
        array (
          'collation' => 'A',
          'null' => false,
        ),
        'option_id' => 
        array (
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
  ),
  'aggregates' => 
  array (
    'Planting' => 
    array (
      'class' => 'earthPlanting',
      'local' => 'planting_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'Feature' => 
    array (
      'class' => 'earthOption',
      'local' => 'option_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
