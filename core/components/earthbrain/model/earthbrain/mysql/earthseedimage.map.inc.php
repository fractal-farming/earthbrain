<?php
/**
 * @package EarthBrain
 */
$xpdo_meta_map['earthSeedImage']= array (
  'package' => 'earthbrain',
  'version' => '1.1',
  'extends' => 'earthImage',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'aggregates' => 
  array (
    'Seed' => 
    array (
      'class' => 'earthSeed',
      'local' => 'object_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
