<?php
/**
 * @package EarthBrain
 */
$xpdo_meta_map['earthSourceEvent']= array (
  'package' => 'earthbrain',
  'version' => '1.1',
  'extends' => 'earthSource',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'aggregates' => 
  array (
    'Event' => 
    array (
      'class' => 'AgendaEvents',
      'local' => 'parent_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
