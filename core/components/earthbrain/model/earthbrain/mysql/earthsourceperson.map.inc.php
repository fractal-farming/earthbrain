<?php
/**
 * @package EarthBrain
 */
$xpdo_meta_map['earthSourcePerson']= array (
  'package' => 'earthbrain',
  'version' => '1.1',
  'extends' => 'earthSource',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'aggregates' => 
  array (
    'Person' => 
    array (
      'class' => 'earthPerson',
      'local' => 'parent_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'PersonData' => 
    array (
      'class' => 'earthPersonData',
      'local' => 'parent_id',
      'foreign' => 'person_id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'UserData' => 
    array (
      'class' => 'modUserProfile',
      'local' => 'parent_id',
      'foreign' => 'internalKey',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
