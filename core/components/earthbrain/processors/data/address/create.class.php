<?php
class earthAddressCreateProcessor extends modObjectCreateProcessor {
    public $classKey = 'earthAddress';
    public $languageTopics = array('earthbrain:default');

    /** @var EarthBrain $earthbrain */
    public $earthbrain;

    private array $earthProcessorProps;

    public function initialize()
    {
        $corePath = $this->modx->getOption('earthbrain.core_path', null, $this->modx->getOption('core_path') . 'components/earthbrain/');
        $this->earthbrain = $this->modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
        $this->earthProcessorProps = ['processors_path' => $this->earthbrain->config['processorsPath']];

        return parent::initialize();
    }

    public function beforeSet()
    {
        // Don't create address if there's no address data (beware of prefilled fields!)
        $exit = true;
        foreach ($this->getProperties() as $key => $value) {
            if (in_array($key, ['country','createdon','createdby','published'])) continue;
            if ($value) $exit = false;
        }
        if ($exit) return parent::success('Not creating new address entry, because no data was present.');

        return parent::beforeSet();
    }
}
return 'earthAddressCreateProcessor';