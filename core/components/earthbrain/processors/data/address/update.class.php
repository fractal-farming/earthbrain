<?php
class earthAddressUpdateProcessor extends modObjectUpdateProcessor {
    public $classKey = 'earthAddress';
    public $languageTopics = array('earthbrain:default');

    /** @var EarthBrain $earthbrain */
    public $earthbrain;

    private array $earthProcessorProps;

    public function initialize()
    {
        $corePath = $this->modx->getOption('earthbrain.core_path', null, $this->modx->getOption('core_path') . 'components/earthbrain/');
        $this->earthbrain = $this->modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
        $this->earthProcessorProps = ['processors_path' => $this->earthbrain->config['processorsPath']];

        return parent::initialize();
    }

    public function beforeSet()
    {
        // Don't update creation date
        $this->unsetProperty('createdon');

        return parent::beforeSet();
    }
    public function beforeSave()
    {
        // Only update edited fields if changes were made
        if ($this->object->_dirty) {
            $this->object->set('editedon', time());
            $this->object->set('editedby', $this->modx->user->get('id'));
        }

        return parent::beforeSave();
    }
}
return 'earthAddressUpdateProcessor';