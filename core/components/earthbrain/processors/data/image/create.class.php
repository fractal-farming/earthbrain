<?php
class earthImageCreateProcessor extends modObjectCreateProcessor {
    public $classKey = 'earthImage';
    public $languageTopics = array('earthbrain:default');

    /** @var EarthBrain $earthbrain */
    public $earthbrain;

    /** @var earthImage $earthimage */
    public $earthimage;

    public function initialize()
    {
        $corePath = $this->modx->getOption('earthbrain.core_path', null, $this->modx->getOption('core_path') . 'components/earthbrain/');
        $this->earthimage = $this->modx->getService('earthimage','earthImage',$corePath . 'model/earthbrain/',array('core_path' => $corePath));

        return parent::initialize();
    }

    public function beforeSet()
    {
        $imgPath = $this->getProperty('path');

        // Create ImagePlus JSON object if no image is set
        if (!$this->getProperty('img') && file_exists($imgPath))
        {
            $size = getimagesize($imgPath);
            $json = json_encode([
                'sourceImg' => [
                    'src' => basename($imgPath),
                    'source' => $this->modx->getOption('earthbrain.img_source', $this->getProperties()),
                    'width' => ($size) ? $size[0] : 0,
                    'height' => ($size) ? $size[1] : 0,
                ],
                'crop' => [
                    'x' => 0,
                    'y' => 0,
                    'width' => ($size) ? $size[0] : 0,
                    'height' => ($size) ? $size[1] : 0,
                ],
                'targetWidth' => null,
                'targetHeight' => null
            ], JSON_PRETTY_PRINT);

            $this->setProperty('img', $json);
            $this->setProperty('img_action', 'create');
        }

        // Increment sort order from current highest position
        $q = $this->modx->newQuery('earthImage');
        $q->select(["max(pos)"]);
        $idx = $this->modx->getValue($q->prepare());

        $this->setProperty('pos', ++$idx);

        return parent::beforeSet();
    }

    public function afterSave()
    {
        $this->earthimage->setImageVariants($this->object);
        //$this->earthimage->extractLocation($this->object);

        return parent::afterSave();
    }
}
return 'earthImageCreateProcessor';