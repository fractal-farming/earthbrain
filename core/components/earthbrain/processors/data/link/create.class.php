<?php
class earthLinkCreateProcessor extends modObjectCreateProcessor {
    public $classKey = 'earthLink';
    public $languageTopics = array('earthbrain:default');
}
return 'earthLinkCreateProcessor';