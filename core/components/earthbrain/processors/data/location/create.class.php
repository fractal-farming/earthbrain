<?php
class earthLocationCreateProcessor extends modObjectCreateProcessor {
    public $classKey = 'earthLocation';
    public $languageTopics = array('earthbrain:default');

    /** @var EarthBrain $earthbrain */
    public $earthbrain;

    private array $earthProcessorProps;

    public function initialize()
    {
        $corePath = $this->modx->getOption('earthbrain.core_path', null, $this->modx->getOption('core_path') . 'components/earthbrain/');
        $this->earthbrain = $this->modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
        $this->earthProcessorProps = ['processors_path' => $this->earthbrain->config['processorsPath']];

        return parent::initialize();
    }

    public function beforeSet()
    {
        // Don't create location if there's no data (beware of prefilled fields!)
        $exit = true;
        foreach ($this->getProperties() as $key => $value) {
            if (in_array($key, ['radius','zoom','createdon','createdby','published'])) continue;
            if ($value) $exit = false;
        }
        if ($exit) return parent::success('Not creating new location entry, because no data was present.');

        // Reset empty null fields
        if (!$this->getProperty('lat')) {
            $this->setProperty('lat', NULL);
        }
        if (!$this->getProperty('lng')) {
            $this->setProperty('lng', NULL);
        }
        if (!$this->getProperty('elevation') && $this->getProperty('elevation') != 0) {
            $this->setProperty('elevation', NULL);
        }
        if (!$this->getProperty('zoom')) {
            $this->setProperty('zoom', NULL);
        }

        return parent::beforeSet();
    }
}
return 'earthLocationCreateProcessor';