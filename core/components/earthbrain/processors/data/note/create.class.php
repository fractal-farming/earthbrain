<?php
class earthNoteCreateProcessor extends modObjectCreateProcessor {
    public $classKey = 'earthNote';
    public $languageTopics = array('earthbrain:default');
}
return 'earthNoteCreateProcessor';