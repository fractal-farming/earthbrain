<?php

require MODX_CORE_PATH . 'model/modx/processors/security/user/create.class.php';

class earthPersonCreateProcessor extends modUserCreateProcessor {
    public $classKey = 'earthPerson';
    public $languageTopics = array('earthbrain:default');
    public $permission = '';

    /** @var EarthBrain $earthbrain */
    public $earthbrain;

    private array $earthProcessorProps;

    public function initialize()
    {
        $corePath = $this->modx->getOption('earthbrain.core_path', null, $this->modx->getOption('core_path') . 'components/earthbrain/');
        $this->earthbrain = $this->modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
        $this->earthProcessorProps = ['processors_path' => $this->earthbrain->config['processorsPath']];

        return parent::initialize();
    }

    public function beforeSet()
    {
        // Generate username if needed
        if (!$this->getProperty('username')) {
            $firstName = $this->getProperty('firstname');
            $lastName = $this->getProperty('lastname');
            $fullName = $this->getProperty('fullname');
            if (!$fullName) $fullName = $firstName . $lastName;

            $userName = strtolower($fullName);
            $userName = preg_replace('/[^A-Za-z0-9 _-]/', '', $userName); // strip non-alphanumeric characters
            $userName = str_replace(' ', '', $userName); // remove whitespace

            // Avoid duplicate name
            $idx = 1;
            while ($userName && $this->modx->getObject('modUser', ['username' => $userName])) {
                $userName = preg_replace('/[^a-z]/', '', $userName);
                $userName = $userName . $idx++;
            }

            // Create random name if value is still empty
            if (!$userName) {
                $userName = 'earthperson' . $this->getProperty('createdon');
            }

            $this->setProperty('username', $userName);
        }

        // Add person to user group (mind the nested array!)
        if (!$this->getProperty('groups')) {
            $groups = [
                [
                    "usergroup" => $this->modx->getOption('earthbrain.usergroup_generated'),
                    "role" => 1,
                ]
            ];
            $this->setProperty('groups', $groups);
        }

        // Reformat phone number
        if ($phone = $this->getProperty('phone')) {
            $this->setProperty('phone', $this->earthbrain->formatPhone($phone));
        }
        if ($mobile = $this->getProperty('mobile')) {
            $this->setProperty('mobilephone', $this->earthbrain->formatPhone($mobile));
        }

        // Ensure email and password are set to avoid errors
        $this->setProperty('passwordnotifymethod', 'x');
        if (!$this->getProperty('email')) {
            $this->setProperty('email', $this->modx->getOption('emailsender'));
        }

        return parent::beforeSet();
    }

    public function afterSave()
    {
        $personData = $this->getProperties();

        // Create or retrieve extended user profile
        if ($this->getProperty('classname') != 'earthPersonData') {
            $earthPersonData = $this->modx->newObject('earthPersonData');
            $earthPersonData->toArray($personData);
        } else {
            $earthPersonData = $this->modx->getObject('earthPersonData', $personData['object_id']);
        }

        // Save address
        $response = $this->modx->runProcessor('data/address/create', $personData['personAddress'], $this->earthProcessorProps);
        if ($response->isError() && !$response->response['message']['success']) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, print_r($response->response, 1));
            return false;
        }
        $address = $response->getObject();

        // Save location
        $response = $this->modx->runProcessor('data/location/create', $personData['personLocation'], $this->earthProcessorProps);
        if ($response->isError() && !$response->response['message']['success']) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, print_r($response->response,1));
            return false;
        }
        $location = $response->getObject();

        // Update person
        $earthPersonData->set('person_id', $this->object->get('id'));
        $earthPersonData->set('address_id', $address['id'] ?? null);
        $earthPersonData->set('location_id', $location['id'] ?? null);
        $earthPersonData->save();

        // Add user ID to generated name
        if ($this->object->get('username') == 'earthperson' . $this->getProperty('createdon')) {
            $this->object->set('username', 'earthperson' . $this->object->get('id'));
            $this->saveObject();
        }

        return parent::afterSave();
    }
}
return 'earthPersonCreateProcessor';