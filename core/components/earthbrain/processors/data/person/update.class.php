<?php

require MODX_CORE_PATH . 'model/modx/processors/security/user/update.class.php';

class earthPersonUpdateProcessor extends modUserUpdateProcessor {
    public $classKey = 'earthPerson';
    public $languageTopics = array('earthbrain:default');
    public $permission = '';

    /** @var EarthBrain $earthbrain */
    public $earthbrain;

    private array $earthProcessorProps;

    public function initialize()
    {
        $corePath = $this->modx->getOption('earthbrain.core_path', null, $this->modx->getOption('core_path') . 'components/earthbrain/');
        $this->earthbrain = $this->modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
        $this->earthProcessorProps = ['processors_path' => $this->earthbrain->config['processorsPath']];

        return parent::initialize();
    }

    public function beforeSet()
    {
        // Reformat phone number
        if ($phone = $this->getProperty('phone')) {
            $this->setProperty('phone', $this->earthbrain->formatPhone($phone));
        }
        if ($mobile = $this->getProperty('mobile')) {
            $this->setProperty('mobilephone', $this->earthbrain->formatPhone($mobile));
        }

        // Ensure email and password are set to avoid errors
        $this->setProperty('passwordnotifymethod', 'x');
        if (!$this->getProperty('email')) {
            $this->setProperty('email', $this->modx->getOption('emailsender'));
        }

        return parent::beforeSet();
    }

    public function afterSave()
    {
        // Update extended user profile
        $personData = $this->getProperties();

        // Update or retrieve extended user profile
        if ($this->getProperty('classname') != 'earthPersonData') {
            $earthPersonData = $this->object->getOne('PersonData');
            $earthPersonData->toArray($personData);
            $earthPersonData->save();
        } else {
            $earthPersonData = $this->modx->getObject('earthPersonData', $personData['object_id']);
        }

        // Create or update address
        if (!$earthPersonData->get('address_id')) {
            $response = $this->modx->runProcessor('data/address/create', $personData['personAddress'], $this->earthProcessorProps);
            $address = $response->getObject();
            $earthPersonData->set('address_id', $address['id'] ?? null);
            $earthPersonData->save();
        } else {
            $personData['personAddress']['id'] = $earthPersonData->get('address_id');
            $response = $this->modx->runProcessor('data/address/update', $personData['personAddress'], $this->earthProcessorProps);
        }
        if ($response->isError() && !$response->response['message']['success']) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, print_r($response->response, 1));
            return false;
        }

        // Create or update location
        if (!$earthPersonData->get('location_id')) {
            $response = $this->modx->runProcessor('data/location/create', $personData['personLocation'], $this->earthProcessorProps);
            $location = $response->getObject();
            $earthPersonData->set('location_id', $location['id'] ?? null);
            $earthPersonData->save();
        } else {
            $personData['personLocation']['id'] = $earthPersonData->get('location_id');
            $response = $this->modx->runProcessor('data/location/update', $personData['personLocation'], $this->earthProcessorProps);
        }
        if ($response->isError() && !$response->response['message']['success']) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, print_r($response->response, 1));
            return false;
        }

        return parent::afterSave();
    }
}
return 'earthPersonUpdateProcessor';