<?php
/**
 * @package earthbrain
 */

namespace FractalFarming\EarthBrain;

use modX;
use Seld\JsonLint\JsonParser;

class EarthBrain
{
    /**
     * A reference to the modX instance
     * @var modX $modx
     */
    public modX $modx;

    /**
     * The namespace
     * @var string $namespace
     */
    public string $namespace = 'earthbrain';

    /**
     * A configuration array
     * @var array $config
     */
    public array $config = [];

    /**
     * Variables for loading MIGX grids
     * @var array $migxProperties
     */
    public array $migxProperties = [];

    /**
     * EarthBrain constructor
     *
     * @param modX $modx A reference to the modX instance.
     * @param array $config An array of configuration options. Optional.
     */
    function __construct(modX &$modx, array $config = [])
    {
        $this->modx =& $modx;

        $corePath = $this->modx->getOption('earthbrain.core_path', $config, $this->modx->getOption('core_path') . 'components/earthbrain/');
        $assetsUrl = $this->modx->getOption('earthbrain.assets_url', $config, $this->modx->getOption('assets_url') . 'components/earthbrain/');

        $this->config = array_merge([
            'basePath' => $this->modx->getOption('base_path'),
            'corePath' => $corePath,
            'modelPath' => $corePath . 'model/',
            'processorsPath' => $corePath . 'processors/',
            'jsUrl' => $assetsUrl . 'js/',
            'cssUrl' => $assetsUrl . 'css/',
            'assetsUrl' => $assetsUrl,
            'connectorUrl' => $assetsUrl . 'connector.php',
        ], $config);

        $this->modx->loadClass('earthTaxon', $this->config['modelPath'] . 'earthbrain/');
        $this->modx->loadClass('earthSeed', $this->config['modelPath'] . 'earthbrain/');
        $this->modx->loadClass('earthPlant', $this->config['modelPath'] . 'earthbrain/');
        $this->modx->loadClass('earthPlanting', $this->config['modelPath'] . 'earthbrain/');
        $this->modx->loadClass('earthPerson', $this->config['modelPath'] . 'earthbrain/');
        $this->modx->loadClass('earthPersonData', $this->config['modelPath'] . 'earthbrain/');
        $this->modx->loadClass('earthSource', $this->config['modelPath'] . 'earthbrain/');
        $this->modx->loadClass('earthAddress', $this->config['modelPath'] . 'earthbrain/');
        $this->modx->loadClass('earthLocation', $this->config['modelPath'] . 'earthbrain/');
        $this->modx->loadClass('earthImage', $this->config['modelPath'] . 'earthbrain/');
        $this->modx->loadClass('earthImageSeed', $this->config['modelPath'] . 'earthbrain/');
        $this->modx->loadClass('earthImagePlant', $this->config['modelPath'] . 'earthbrain/');
        $this->modx->loadClass('earthImageSource', $this->config['modelPath'] . 'earthbrain/');
        $this->modx->loadClass('earthNote', $this->config['modelPath'] . 'earthbrain/');
        $this->modx->loadClass('earthNoteTaxon', $this->config['modelPath'] . 'earthbrain/');
        $this->modx->loadClass('earthNoteSeed', $this->config['modelPath'] . 'earthbrain/');
        $this->modx->loadClass('earthNotePlant', $this->config['modelPath'] . 'earthbrain/');
        $this->modx->loadClass('earthNoteSource', $this->config['modelPath'] . 'earthbrain/');
        $this->modx->loadClass('earthLink', $this->config['modelPath'] . 'earthbrain/');
        $this->modx->loadClass('earthLinkTaxon', $this->config['modelPath'] . 'earthbrain/');

        $this->modx->addPackage('earthbrain', $this->config['modelPath']);

        // Custom properties for earthObjectMediaPath
        $this->migxProperties = array_merge([
            'earthbrain_taxonomy:earthbrain' => [
                'category' => 'taxon',
                'classKeys' => [
                    'note' => 'earthNoteTaxon',
                    'link' => 'earthLinkTaxon',
                ]
            ],
            'earthbrain_seeds:earthbrain' => [
                'category' => 'seed',
                'classKeys' => [
                    'image' => 'earthImageSeed',
                    'note' => 'earthNoteSeed',
                ]
            ],
            'earthbrain_plants:earthbrain' => [
                'category' => 'plant',
                'classKeys' => [
                    'image' => 'earthImagePlant',
                    'note' => 'earthNotePlant',
                ]
            ],
            'earthbrain_plantings:earthbrain' => [
                'category' => 'plant',
                'className' => 'earthPlanting', // To share image path with plant parent
                'classKeys' => [
                    'image' => 'earthImagePlanting',
                    'note' => 'earthNotePlanting',
                ],
                'fieldName' => 'plant_id'
            ],
            'earthbrain_sources:earthbrain' => [
                'category' => 'source',
                'classKeys' => [
                    'image' => 'earthImageSource',
                    'note' => 'earthNoteSource',
                    'link' => 'earthLinkSource',
                ]
            ],
            'earthbrain_exchanges:earthbrain' => [
                'category' => 'exchange',
                'classKeys' => [
                    'image' => 'earthImageExchange',
                    'note' => 'earthNoteExchange',
                ]
            ]
        ]);
    }

    /**
     * Get MIGX properties for loading linked objects in correct parent grid.
     *
     * @param string $config
     * @return array|false
     */
    public function getMigxProperties(string $config): false|array
    {
        if (array_key_exists($config, $this->migxProperties)) {
            return $this->migxProperties[$config];
        }
        return false;
    }

    /**
     * Add MIGX properties from other packages to the default array.
     *
     * @param array $configs
     * @return void
     */
    public function addMigxProperties(array $configs): void
    {
        $this->migxProperties = array_merge($this->migxProperties, $configs);
    }

    /**
     * Prevent NULL values from being set to 0 in MIGXdb grids.
     *
     * @param object $object
     * @param array $properties
     * @param string $prefix
     * @return void
     */
    public function resetNULL(object $object, array $properties, string $prefix = ''): void
    {
        foreach ($properties as $key => $value) {
            $key = str_replace($prefix, '', $key);
            $objectValue = $object->get($key);

            // Reset to NULL if property value is empty and object value is 0
            if ($objectValue === 0 && $value === '') {
                $this->modx->log(modX::LOG_LEVEL_INFO, 'NULL was reset for: ' . $key);
                $object->set($key, NULL);
            }

            // Reset empty lat/long fields
            if (in_array($key, ['lat', 'lng']) && $value === '') {
                $this->modx->log(modX::LOG_LEVEL_INFO, 'NULL was reset for: ' . $key);
                $object->set($key, NULL);
            }
        }
        $object->save();
    }

    /**
     * Create and edit locations inside referencing MIGXdb configs.
     *
     * @param object $object
     * @param array $properties
     * @return void
     */
    public function saveLocation(object $object, array $properties): void
    {
        // Create or update location (does nothing if array is empty)
        $action = $object->get('location_id') ? 'update' : 'create';

        $response = $this->modx->runProcessor('data/location/' . $action,
            [
                'id' => $properties['Location_id'] ?? '',
                'lat' => $properties['Location_lat'] ?? null,
                'lng' => $properties['Location_lng'] ?? null,
                'elevation' => $properties['Location_elevation'] ?? null,
                'radius' => $properties['Location_radius'] ?? $this->modx->getOption('earthbrain.location_radius_default', null, 0),
                'zoom' => $properties['Location_zoom'] ?? null,
                'geojson' => $properties['Location_geojson'] ?? '',
                'kml' => $properties['Location_kml'] ?? '',
                'createdon' => time(),
                'createdby' => $object->get('createdby') ?? $this->modx->user->get('id'),
                'published' => $object->get('published') ?? 0,
            ],[
                'processors_path' => $this->config['processorsPath']
            ]
        );

        if ($response->isError())
        {
            // For reasons beyond logic, error responses can contain success messages
            if (isset($response->response['message']['success'])) {
                $this->modx->log(modX::LOG_LEVEL_INFO, $response->response['message']['message']);
                $this->modx->error->reset(); // Thanks HACKERMAN!!!
            } else {
                $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
                return;
            }
        }
        $location = $response->getObject();

        // Attach location to object
        if ($location && $action === 'create'){
            $object->set('location_id', $location['id']);
            $object->save();
        }
    }

    /**
     * Create and edit addresses inside referencing MIGXdb configs.
     *
     * @param object $object
     * @param array $properties
     * @return void
     */
    public function saveAddress(object $object, array $properties): void
    {
        // Create or update location (does nothing if array is empty)
        $action = $object->get('address_id') ? 'update' : 'create';

        $response = $this->modx->runProcessor('data/address/' . $action,
            [
                'id' => $properties['Address_id'] ?? '',
                'line_1' => $properties['Address_line_1'] ?? '',
                'line_2' => $properties['Address_line_2'] ?? '',
                'line_3' => $properties['Address_line_3'] ?? '',
                'locality' => $properties['Address_locality'] ?? '',
                'region' => $properties['Address_region'] ?? '',
                'country' => $properties['Address_country'] ?? '',
                'postal_code' => $properties['Address_postal_code'] ?? '',
                'comments' => $properties['Address_comments'] ?? '',
                'createdon' => time(),
                'createdby' => $object->get('createdby') ?? $this->modx->user->get('id'),
                'published' => $object->get('published') ?? 0,
            ],[
                'processors_path' => $this->config['processorsPath']
            ]
        );

        if ($response->isError())
        {
            // For reasons beyond logic, error responses can contain success messages
            if (isset($response->response['message']['success'])) {
                $this->modx->log(modX::LOG_LEVEL_INFO, $response->response['message']['message']);
                $this->modx->error->reset(); // Thanks HACKERMAN!!!
            } else {
                $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
                return;
            }
        }
        $address = $response->getObject();

        // Attach address to object
        if ($address && $action === 'create'){
            $object->set('address_id', $address['id']);
            $object->save();
        }
    }

    /**
     * Reformat phone number.
     *
     * @param string $phone
     * @return string
     */
    public function formatPhone(string $phone)
    {
        // Reformat phone number
        $phone = strip_tags($phone); // strip HTML
        $phone = str_replace('+63', '0', $phone); // replace country prefix
        $phone = preg_replace('/[^0-9]/', '', $phone); // strip non-numeric characters
        $phone = preg_replace('/\s+/', '', $phone); // strip whitespace

        return $phone;
    }

    /**
     * Try to find a person by phone number.
     *
     * @param string $phone
     * @param bool $personNew
     * @return string[]|int
     */
    public function getPerson(string $phone, bool $personNew)
    {
        $phone = $this->formatPhone($phone);
        $person = $this->modx->getObject('modUserProfile', ['phone' => $phone]);

        // Abort if existing user can't be matched by phone number
        if (!$personNew && !is_object($person)) {
            return ['error' => "We couldn't find this phone number in our database."];
        }

        // Reversely, abort if new user entered an existing phone number
        if ($personNew && is_object($person)) {
            return ['error' => "This phone number already exists in our database! Are you sure this is your first submission?"];
        }

        // Get existing user ID
        if (is_object($person)) {
            return (int)$person->get('internalKey');
        }

        return [];
    }

    /**
     * Increment sort order of new items in MIGX grids.
     *
     * @param object $object
     * @param array $properties
     * @return void
     */
    public function incrementPosition(object $object, array $properties): void
    {
        if ($properties['object_id'] === 'new') {
            $q = $this->modx->newQuery($properties['class_key']);
            $q->select(["max(pos)"]);
            $lastPosition = $this->modx->getValue($q->prepare());
            $object->set('pos', ++$lastPosition);
        }
    }

    /**
     * Validate JSON.
     *
     * @param string $json
     * @return array|bool
     */
    public function validateJSON(string $json)
    {
        $parser = new JsonParser();

        // Result will be empty on success
        $validateOutput = $parser->lint($json);

        if ($validateOutput) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, '[EarthBrain] ' . $validateOutput);
            return ['error' => 'JSON not valid. See error log for details.'];
        }

        return true;
    }
}
