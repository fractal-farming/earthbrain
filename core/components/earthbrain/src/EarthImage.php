<?php
/**
 * @package EarthBrain
 */

namespace FractalFarming\EarthBrain;

use Location\Factory\CoordinateFactory;
use Location\Formatter\Coordinate\DecimalDegrees;
use modX;
use xPDOSimpleObject;

class EarthImage extends xPDOSimpleObject
{
    /** @var modX $xpdo */
    public $xpdo;

    /**
     * Create variants with different aspect ratios for given image object.
     *
     * @param $object
     * @return void
     */
    public function setImageVariants($object): void
    {
        // Get properties from image
        $img = json_decode($object->get('img'), true);

        // Autofill image variants when image is created or replaced
        if (is_array($img) && $object->get('img_action') === 'create') {
            if (!$object->get('path') || $object->get('path') !== $img['sourceImg']['src']) {
                $object->set('path', $img['sourceImg']['src']);
            }

            $imgWidth = $img['sourceImg']['width'];
            $imgHeight = $img['sourceImg']['height'];
            $orientation = 'landscape';
            if ($imgWidth < $imgHeight) {
                $orientation = 'portrait';
            }

            // Portrait
            if ($orientation == 'landscape') {
                $img['crop']['width'] = round($imgHeight / 1.33333);
                $img['crop']['height'] = $imgHeight;
            }
            if ($orientation == 'portrait') {
                $img['crop']['height'] = min($imgHeight, round($imgWidth * 1.33333));
                $img['crop']['width'] = min($imgWidth, round($imgHeight / 1.33333));
            }
            $img['crop']['x'] = ($imgWidth - $img['crop']['width']) / 2;
            $img['crop']['y'] = ($imgHeight - $img['crop']['height']) / 2;

            $object->set('img_portrait', json_encode($img));
            //$this->xpdo->log(modX::LOG_LEVEL_ERROR, print_r($img,1));

            // Landscape
            if ($orientation == 'landscape') {
                $img['crop']['width'] = min($imgWidth, round($imgHeight * 1.33333));
                $img['crop']['height'] = min($imgHeight, round($imgWidth / 1.33333));
            }
            if ($orientation == 'portrait') {
                $img['crop']['width'] = $imgWidth;
                $img['crop']['height'] = round($imgWidth / 1.33333);
            }
            $img['crop']['x'] = ($imgWidth - $img['crop']['width']) / 2;
            $img['crop']['y'] = ($imgHeight - $img['crop']['height']) / 2;

            $object->set('img_landscape', json_encode($img));
            //$this->xpdo->log(modX::LOG_LEVEL_ERROR, print_r($img,1));

            // Square
            $img['crop']['width'] = min($imgHeight, $imgWidth);
            $img['crop']['height'] = $img['crop']['width'];
            $img['crop']['x'] = ($imgWidth - $img['crop']['width']) / 2;
            $img['crop']['y'] = ($imgHeight - $img['crop']['height']) / 2;

            $object->set('img_square', json_encode($img));
            //$this->xpdo->log(modX::LOG_LEVEL_ERROR, print_r($img,1));

            // Wide
            if ($orientation == 'landscape') {
                $img['crop']['width'] = min($imgWidth, round($imgHeight * 1.77777));
                $img['crop']['height'] = min($imgHeight, round($imgWidth / 1.77777));
            }
            if ($orientation == 'portrait') {
                $img['crop']['width'] = $imgWidth;
                $img['crop']['height'] = round($imgWidth / 1.77777);
            }
            $img['crop']['x'] = ($imgWidth - $img['crop']['width']) / 2;
            $img['crop']['y'] = ($imgHeight - $img['crop']['height']) / 2;

            $object->set('img_wide', json_encode($img));
            //$this->xpdo->log(modX::LOG_LEVEL_ERROR, print_r($img,1));

            // Panorama
            if ($orientation == 'landscape') {
                $img['crop']['width'] = min($imgWidth, round($imgHeight * 2.33333));
                $img['crop']['height'] = min($imgHeight, round($imgWidth / 2.33333));
            }
            if ($orientation == 'portrait') {
                $img['crop']['width'] = $imgWidth;
                $img['crop']['height'] = round($imgWidth / 2.33333);
            }
            $img['crop']['x'] = ($imgWidth - $img['crop']['width']) / 2;
            $img['crop']['y'] = ($imgHeight - $img['crop']['height']) / 2;

            $object->set('img_pano', json_encode($img));
            $object->save();
        }

        if (!is_array($img) || $object->get('img_action') === 'remove') {
            $object->set('path', '');
            $object->set('img_portrait', '');
            $object->set('img_landscape', '');
            $object->set('img_square', '');
            $object->set('img_wide', '');
            $object->set('img_pano', '');
            $object->save();
        }
    }

    /**
     * Fix media source path for (overview) image.
     *
     * The dynamically generated path used by the EarthImage media source does
     * not include the correct category and parent ID values when used outside
     * MIGX.
     *
     * This function overrides the current path (relative to the media source)
     * with a path relative to the site root (the filesystem source with ID 1).
     *
     * Usage:
     * Either define an img_path manually in your array (from project root up to
     * the path value inside the image object), or provide the class_key and
     * parent_id values for the object.
     *
     * @param array $img
     * @return string
     */
    public function fixSourcePath(array $img): string
    {
        $imgPath = $img['img_path'] ?? null;

        // Cook something up if no image path was provided
        if (!$imgPath) {
            $category = preg_replace('/^[a-z]+Image/', '', $img['class_key']);
            $category = strtolower($category);

            // Some exceptions slip through the above regex, i.e. forestImage
            if (!$category) {
                $category = preg_replace('/Image/', '', $img['class_key']);
            }

            $imgPath = 'uploads/img/' . $category . '/' . $img['parent_id'] . '/';
        }

        // Fall back to root media source and insert custom path
        $img['sourceImg']['src'] = $imgPath . $img['sourceImg']['src'];
        $img['sourceImg']['source'] = 1;

        return json_encode($img);
    }

    /**
     * Extract EXIF data from an image.
     *
     * Source image can be an ImagePlus object (provided as JSON string), a
     * relative path from project root, or a relative path from a media source
     * root (provide the media source ID if that's the case).
     *
     * It is possible to configure the media source to save multiple versions of
     * an image. One of the objectives could be to strip the EXIF data from the
     * public version, and store this data in a copied version instead.
     *
     * This function looks for a copy of the image file in a subfolder named
     * _meta. If it exists, the EXIF is retrieved from that version. If not,
     * then the origin version will be probed.
     *
     * @param string $img JSON object or path
     * @param int|null $source Optional
     * @return array
     */
    public function getExifData(string $img, int $source = null): array
    {
        $imgPlus = json_decode($img, true);
        $path = $img;

        if (json_last_error() === JSON_ERROR_NONE) {
            $path = $imgPlus['sourceImg']['src'];
            $source = $imgPlus['sourceImg']['source'];
        }

        if (!extension_loaded('exif')) {
            $this->xpdo->log(modX::LOG_LEVEL_ERROR, 'PHP Exif extension is not enabled!');
            return [];
        }

        // Generate full path from media source
        if ($mediaSource = $this->xpdo->getObject('sources.modMediaSource', $source)) {
            $path = $mediaSource->prepareOutputUrl($path);
        } else {
            $this->xpdo->log(modX::LOG_LEVEL_ERROR, "Media source $source could not be connected!");
        }

        // Attempt to read metadata from copied version
        $basePath = pathinfo($path, PATHINFO_DIRNAME);
        $fileName = pathinfo($path, PATHINFO_BASENAME);
        $metaPath = $basePath . '/_meta/' . $fileName;
        $fullPath = MODX_BASE_PATH . $metaPath;

        // Fall back on original
        if (!file_exists($fullPath)) {
            $fullPath = MODX_BASE_PATH . $path;
        }
        if (!file_exists($fullPath)) {
            $this->xpdo->log(modX::LOG_LEVEL_ERROR, "File not found: $fullPath");
            return [];
        }

        $exifData = exif_read_data($fullPath);
        if (!$exifData) {
            $this->xpdo->log(modX::LOG_LEVEL_ERROR, "Exif data could not be read from: $fullPath");
            return [];
        }

        $exifJSON = json_encode($exifData, JSON_INVALID_UTF8_SUBSTITUTE | JSON_UNESCAPED_SLASHES);

        // Attempt to extract geo coordinates
        if (str_contains($exifData["SectionsFound"], 'GPS')) {
            $lat = $this->formatDMSArray($exifData['GPSLatitude'], $exifData['GPSLatitudeRef']);
            $lng = $this->formatDMSArray($exifData['GPSLongitude'], $exifData['GPSLongitudeRef']);

            // Set altitude
            $alt = $exifData['GPSAltitude'] ?? null;
            $altDivided = explode("/", $alt);
            if (isset($altDivided[1])) {
                $alt = $altDivided[0] / $altDivided[1];
                $alt = round($alt, 1);
            }

            // Check if altitude is below sea level
            $altRef = (int)bin2hex($exifData['GPSAltitudeRef']);
            if ($altRef === 1) {
                $alt = "-$alt";
            }
        }

        // Set date that picture was taken
        if (isset($exifData['DateTimeOriginal'])) {
            $date = strtotime($exifData['DateTimeOriginal']);
        }

        //$this->xpdo->log(modX::LOG_LEVEL_ERROR, $path);
        //$this->xpdo->log(modX::LOG_LEVEL_ERROR, $object->get('path'));
        //$this->xpdo->log(modX::LOG_LEVEL_ERROR, print_r($exifData,1));

        return [
            'lat' => $lat ?? null,
            'lng' => $lng ?? null,
            'elevation' => $alt ?? null,
            'date' => $date ?? null,
            'json' => $exifJSON
        ];
    }

    private function formatDMSArray($coordinate, $direction): string
    {
        if (is_string($coordinate)) {
            $coordinate = array_map("trim", explode(",", $coordinate));
        }
        for ($i = 0; $i < 3; $i++) {
            $part = explode('/', $coordinate[$i]);
            if (count($part) == 1) {
                $coordinate[$i] = $part[0];
            } else if (count($part) == 2) {
                $coordinate[$i] = floatval($part[0]) / floatval($part[1]);
            } else {
                $coordinate[$i] = 0;
            }
        }

        list($degrees, $minutes, $seconds) = $coordinate;
        $sign = ($direction == 'W' || $direction == 'S') ? -1 : 1;
        return $sign * ($degrees + $minutes / 60 + $seconds / 3600);
    }

    public function convertDMSToDecimal($coordinates): string
    {
        $point = CoordinateFactory::fromString($coordinates);
        return $point->format(new DecimalDegrees(','));
    }
}