<?php
/**
 * @package EarthBrain
 */

namespace FractalFarming\EarthBrain;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

//use Geocoder\Provider\Mapbox\Mapbox;
use Geocoder\Provider\LocationIQ\LocationIQ;
use Geocoder\Query\GeocodeQuery;
use Geocoder\Query\ReverseQuery;
use Geocoder\Exception\Exception;
use Geocoder\Dumper\GeoArray;
use modX;
use xPDO;
use xPDOSimpleObject;

class EarthLocation extends xPDOSimpleObject
{
    /** @var modX $xpdo */
    public $xpdo;

    /**
     * Pick random location within a given radius
     *
     * Given a $center (latitude,longitude) coordinates and a distance $radius
     * (kilometers), returns a random point (latitude,longitude) which is within
     * $radius kilometers of $center.
     *
     * @param array $center Numeric array of floats. First element is latitude,
     *                       second is longitude.
     * @param float $radius The radius (in kilometers).
     * @return array         Array of floats (lat/lng).
     *
     * @link   https://stackoverflow.com/a/21646258
     */
    public function obfuscateCoordinates(array $center, float $radius): array
    {
        $radius_earth = 6371; // kilometers

        // Pick random distance within $distance
        $distance = lcg_value() * $radius;

        // Convert degrees to radians
        $center_rads = array_map('deg2rad', $center);

        // First suppose our point is the North Pole.
        // Find a random point $distance kilometers away.
        $lat_rads = (pi() / 2) - $distance / $radius_earth;
        $lng_rads = lcg_value() * 2 * pi();

        // ($lat_rads,$lng_rads) is a point on the circle which is $distance
        // kilometers from the North Pole. Convert to Cartesian.
        $x1 = cos($lat_rads) * sin($lng_rads);
        $y1 = cos($lat_rads) * cos($lng_rads);
        $z1 = sin($lat_rads);

        // Rotate the sphere so that the North Pole is now at $center.
        // Rotate in x axis by $rot = (pi()/2) - $center_rads[0].
        $rot = (pi() / 2) - $center_rads[0];
        $x2 = $x1;
        $y2 = $y1 * cos($rot) + $z1 * sin($rot);
        $z2 = -$y1 * sin($rot) + $z1 * cos($rot);

        // Rotate in z axis by $rot = $center_rads[1]
        $rot = $center_rads[1];
        $x3 = $x2 * cos($rot) + $y2 * sin($rot);
        $y3 = -$x2 * sin($rot) + $y2 * cos($rot);
        $z3 = $z2;

        // Finally, convert this point to polar coordinates
        $lng_rads = atan2($x3, $y3);
        $lat_rads = asin($z3);

        return array_map('rad2deg', [
            'lat' => $lat_rads,
            'lng' => $lng_rads
        ]);
    }

    /**
     * Initialize geocoder
     */
    private function initGeocoder()
    {
        $config = [
            'timeout' => $this->xpdo->getOption('earthbrain.location_geocode_timeout'),
            'verify' => true,
        ];
        $locationIQAccessToken = $this->xpdo->getOption('earthbrain.locationiq_access_token');
        $locationIQRegion = $this->xpdo->getOption('earthbrain.locationiq_region');
        //$mapboxAccessToken = $this->xpdo->getOption('earthbrain.mapbox_access_token');

        $httpClient = new Client($config);
        $geocoder = new LocationIQ($httpClient, $locationIQAccessToken, $locationIQRegion);
        //$geocoder = new Mapbox($httpClient, $mapboxAccessToken);

        return $geocoder;
    }

    /**
     * Convert street address to coordinates.
     * @param string $address
     * @param string $countryCode
     * @return array
     */
    public function geocodeAddress(string $address, string $countryCode = ''): array
    {
        if (!$address) {
            $this->xpdo->log(modX::LOG_LEVEL_ERROR, '[earthLocation] No address was provided.');
            return ['error' => 'No address was provided!'];
        }

        if (extension_loaded('intl')) {
            $country = Locale::getDisplayRegion('-' . $countryCode, 'en');
        } else {
            $this->xpdo->log(modX::LOG_LEVEL_ERROR, '[earthLocation] The php-intl extension is not loaded!');
            return ['error' => 'The php-intl extension is required, but not loaded!'];
        }

        // Remove breaks from string
        $address = str_replace("\r\n", ', ', $address);

        // Append country, if present
        if ($country) $address = "$address, $country";

        // Cache results, to prevent unnecessary API requests
        $cacheManager = $this->xpdo->getCacheManager();
        $cacheKey = 'geocoder';
        $cacheElementKey = 'locations/' . md5(json_encode($address));
        $cacheLifetime = 86400 * 365;
        $cacheOptions = [
            xPDO::OPT_CACHE_KEY => $cacheKey,
            xPDO::OPT_CACHE_EXPIRES => $cacheLifetime,
        ];

        // Check the cache first
        $location = $cacheManager->get($cacheElementKey, $cacheOptions);

        // If nothing was cached, we code some geo
        if (!$location) {
            try {
                $geocoder = $this->initGeocoder();
                $dumper = new GeoArray();
                $location = $geocoder->geocodeQuery(GeocodeQuery::create($address));
                $location = $dumper->dump($location->first());
            } catch (GuzzleException $e) {
                $this->xpdo->log(modX::LOG_LEVEL_ERROR, '[earthLocation] ' . $e->getMessage());
                return ['error' => 'Fuzzle my Guzzle! Are you connected to the internet?'];
            } catch (Exception $e) {
                $this->xpdo->log(modX::LOG_LEVEL_ERROR, '[earthLocation] Error ' . $e->getCode() . ': ' . $e->getMessage());
                return ['error' => $e->getMessage()];
            }

            // Cache result
            $cacheManager->set($cacheElementKey, $location, $cacheLifetime, $cacheOptions);
        }

        return [
            'Location_lat' => $location['geometry']['coordinates'][1],
            'Location_lng' => $location['geometry']['coordinates'][0],
            'Address_line_1' => trim($location['properties']['streetNumber'] . ' ' . $location['properties']['streetName']),
            'Address_line_2' => '',
            'Address_line_3' => $location['properties']['subLocality'] ?? '',
            'Address_locality' => $location['properties']['locality'] ?? '',
            'Address_region' => $location['properties']['adminLevels'][1]['name'] ?? '',
            'Address_country' => $location['properties']['countryCode'] ?? '',
            'Address_postal_code' => $location['properties']['postalCode'] ?? '',
        ];
    }

    /**
     * Convert coordinates to an address.
     *
     * @param float|null $lat
     * @param float|null $lng
     * @return array
     */
    public function geocodeAddressReverse(float $lat = null, float $lng = null): array
    {
        if (!$lat || !$lng) {
            $this->xpdo->log(modX::LOG_LEVEL_ERROR, '[earthLocation] No coordinates were provided.');
            return ['error' => 'No coordinates were provided!'];
        }

        // Cache results, to prevent unnecessary API requests
        $cacheManager = $this->xpdo->getCacheManager();
        $cacheKey = 'geocoder';
        $cacheElementKey = 'locations/' . md5(json_encode($lat . $lng));
        $cacheLifetime = 86400 * 365;
        $cacheOptions = [
            xPDO::OPT_CACHE_KEY => $cacheKey,
            xPDO::OPT_CACHE_EXPIRES => $cacheLifetime,
        ];

        // Check the cache first
        $location = $cacheManager->get($cacheElementKey, $cacheOptions);

        // If nothing was cached, we code some geo
        if (!$location) {
            try {
                $geocoder = $this->initGeocoder();
                $dumper = new GeoArray();
                $location = $geocoder->reverseQuery(ReverseQuery::fromCoordinates($lat, $lng));
                $location = $dumper->dump($location->first());
            } catch (GuzzleException $e) {
                $this->xpdo->log(modX::LOG_LEVEL_ERROR, '[earthLocation] ' . $e->getMessage());
                return ['error' => 'Fuzzle my Guzzle! Are you connected to the internet?'];
            } catch (Exception $e) {
                $this->xpdo->log(modX::LOG_LEVEL_ERROR, '[earthLocation] Error ' . $e->getCode() . ': ' . $e->getMessage());
                return ['error' => $e->getMessage()];
            }

            // Cache result
            $cacheManager->set($cacheElementKey, $location, $cacheLifetime, $cacheOptions);
        }

        $result = [
            'Address_line_1' => trim($location['properties']['streetNumber'] . ' ' . $location['properties']['streetName']),
            'Address_line_2' => '',
            'Address_line_3' => $location['properties']['subLocality'] ?? '',
            'Address_locality' => $location['properties']['locality'] ?? '',
            'Address_region' => $location['properties']['adminLevels'][1]['name'] ?? '',
            'Address_country' => $location['properties']['countryCode'] ?? '',
            'Address_postal_code' => $location['properties']['postalCode'] ?? '',
        ];

        return $result;

        //$this->xpdo->log(modX::LOG_LEVEL_ERROR, print_r($location,1));
    }
}